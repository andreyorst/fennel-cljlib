(require-macros :io.gitlab.andreyorst.cljlib.core)
(ns core
  "MIT License

Copyright (c) 2022 Andrey Listopadov

Permission is hereby granted‚ free of charge‚ to any person obtaining a copy
of this software and associated documentation files (the “Software”)‚ to deal
in the Software without restriction‚ including without limitation the rights
to use‚ copy‚ modify‚ merge‚ publish‚ distribute‚ sublicense‚ and/or sell
copies of the Software‚ and to permit persons to whom the Software is
furnished to do so‚ subject to the following conditions：

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”‚ WITHOUT WARRANTY OF ANY KIND‚ EXPRESS OR
IMPLIED‚ INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY‚
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM‚ DAMAGES OR OTHER
LIABILITY‚ WHETHER IN AN ACTION OF CONTRACT‚ TORT OR OTHERWISE‚ ARISING FROM‚
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."
  (:require [io.gitlab.andreyorst.lazy-seq :as lazy]
            [io.gitlab.andreyorst.itable :as itable]
            [io.gitlab.andreyorst.reduced :as rdc]
            [io.gitlab.andreyorst.inst :as lua-inst]
            [io.gitlab.andreyorst.async :as a]
            [io.gitlab.andreyorst.uuid :as uuid]
            [fennel :refer [view]]))

(set core.__VERSION "0.1.241")

;;; Utility functions

(fn unpack* [x ...]
  (if (core.seq? x)
      (lazy.unpack x)
      (itable.unpack x ...)))

(fn pack* [...]
  (doto [...] (tset :n (select "#" ...))))

(fn pairs* [t]
  (case (getmetatable t)
    {:__pairs p} (p t)
    _ (pairs t)))

(fn ipairs* [t]
  (case (getmetatable t)
    {:__ipairs i} (i t)
    _ (ipairs t)))

(fn length* [t]
  (case (getmetatable t)
    {:__len l} (l t)
    _ (length t)))

(defn apply
  "Apply `f` to the argument list formed by prepending intervening
arguments to `args`, and `f` must support variadic amount of
arguments.

# Examples
Applying `add` to different amount of arguments:

``` fennel
(assert-eq (apply add [1 2 3 4]) 10)
(assert-eq (apply add 1 [2 3 4]) 10)
(assert-eq (apply add 1 2 3 4 5 6 [7 8 9]) 45)
```"
  ([f args] (f (unpack* args)))
  ([f a args] (f a (unpack* args)))
  ([f a b args] (f a b (unpack* args)))
  ([f a b c args] (f a b c (unpack* args)))
  ([f a b c d & args]
   (let [flat-args []
         len (- (length* args) 1)]
     (for [i 1 len]
       (tset flat-args i (. args i)))
     (each [i a (pairs* (. args (+ len 1)))]
       (tset flat-args (+ i len) a))
     (f a b c d (unpack* flat-args)))))

(defn add
  "Sum arbitrary amount of numbers."
  ([] 0)
  ([a] a)
  ([a b] (+ a b))
  ([a b c] (+ a b c))
  ([a b c d] (+ a b c d))
  ([a b c d & rest] (apply add (+ a b c d) rest)))

(set core.+ add)

(defn sub
  "Subtract arbitrary amount of numbers."
  ([] 0)
  ([a] (- a))
  ([a b] (- a b))
  ([a b c] (- a b c))
  ([a b c d] (- a b c d))
  ([a b c d & rest] (apply sub (- a b c d) rest)))

(set core.- sub)

(defn mul
  "Multiply arbitrary amount of numbers."
  ([] 1)
  ([a] a)
  ([a b] (* a b))
  ([a b c] (* a b c))
  ([a b c d] (* a b c d))
  ([a b c d & rest] (apply mul (* a b c d) rest)))

(set core.* mul)

(defn div
  "Divide arbitrary amount of numbers."
  ([a] (/ 1 a))
  ([a b] (/ a b))
  ([a b c] (/ a b c))
  ([a b c d] (/ a b c d))
  ([a b c d & rest] (apply div (/ a b c d) rest)))

(set core./ core.div)

(fn* core.rem
  "remainder of dividing `numerator` by `denominator`."
  [numerator denominator]
  (assert (not= 0 denominator) "division by zero")
  (% numerator denominator))

(fn* core.quot
  "quot[ient] of dividing `numerator` by `denominator`."
  [numerator denominator]
  (assert (not= 0 denominator) "division by zero")
  (math.floor (/ numerator denominator)))

;; TODO: why it is the same as quot?
(fn* core.mod
  "Modulus of `num` and `div`. Truncates toward negative infinity."
  [num div]
  (assert (not= 0 div) "division by zero")
  (math.floor (/ num div)))

(defn ==
  "Returns non-nil if nums all have the equivalent value, otherwise false"
  ([_] true)
  ([a b]
   (assert (and (= :number (type a))
                (= :number (type b)))
           "all operands must be numbers")
   (rawequal a b))
  ([a b & [c d  & more]]
   (if (== a b)
       (if d (apply == b c d more)
           (== b c))
       false)))

(defn le
  "Returns true if nums are in monotonically non-decreasing order"
  ([_] true)
  ([a b] (<= a b))
  ([a b & [c d & more]]
   (if (<= a b)
       (if d (apply le b c d more)
           (<= b c))
       false)))

(set core.<= le)

(defn lt
  "Returns true if nums are in monotonically decreasing order"
  ([_] true)
  ([a b] (< a b))
  ([a b & [c d & more]]
   (if (< a b)
       (if d (apply lt b c d more)
           (< b c))
       false)))

(set core.< lt)

(defn ge
  "Returns true if nums are in monotonically non-increasing order"
  ([_] true)
  ([a b] (>= a b))
  ([a b & [c d & more]]
   (if (>= a b)
       (if d (apply ge b c d more)
           (>= b c))
       false)))

(set core.>= ge)

(defn gt
  "Returns true if nums are in monotonically increasing order"
  ([_] true)
  ([a b] (> a b))
  ([a b & [c d & more]]
   (if (> a b)
       (if d (apply gt b c d more)
           (> b c))
       false)))

(set core.> gt)

(fn* core.inc
  "Increase number `x` by one"
  [x]
  (+ x 1))

(fn* core.dec
  "Decrease number `x` by one"
  [x]
  (- x 1))

(set core.max math.max)
(set core.min math.min)

(fn* core.float?
  "Returns true if `n` is a floating point number (or a zero)."
  [n]
  (or (not= (- n (math.floor n)) n) (= 0 n)))

(fn* core.nat-int?
  "Return true if `x` is a non-negative fixed precision integer"
  [x]
  (and (or (not (core.float? x)) (= 0 x)) (>= x 0)))

(fn* core.rand-int
  "Returns a random integer between `0` (inclusive) and `n` (exclusive)."
  [n]
  (if (< n 0)
      (+ (math.random (math.ceil n) -1) 1)
      (- (math.random (math.ceil (+ n 1))) 1)))

(fn* core.rand
  "Returns a random floating point number between `0` (inclusive) and
  `n` (default `1`) (exclusive)."
  ([] (math.random))
  ([n] (if (< n 0)
           (- (core.rand-int n) (math.random))
           (+ (core.rand-int n) (math.random)))))

(fn* core.int
  "Coerce `x` to int."
  [x]
  (assert (= :number (type x)) "argument must be a number")
  x)

(fn* core.int
  "Coerce `x` to int."
  [x]
  (assert (= :number (type x)) "argument must be a number")
  x)

(defn class
  "Return cljlib type of the `x`, or lua type."
  [x]
  (case (type x)
    :table (case (getmetatable x)
             {:cljlib/type t} t
             _ :table)
    t t))

(defn constantly
  "Returns a function that takes any number of arguments and returns `x`."
  [x]
  (fn [] x))

(defn complement
  "Takes a function `f` and returns the function that takes the same
amount of arguments as `f`, has the same effect, and returns the
opposite truth value."
  [f]
  (fn*
    ([] (not (f)))
    ([a] (not (f a)))
    ([a b] (not (f a b)))
    ([a b & cs] (not (apply f a b cs)))))

(defn identity
  "Returns its argument."
  [x]
  x)

(defn comp
  "Compose functions."
  ([] identity)
  ([f] f)
  ([f g]
   (fn*
     ([] (f (g)))
     ([x] (f (g x)))
     ([x y] (f (g x y)))
     ([x y z] (f (g x y z)))
     ([x y z & args] (f (apply g x y z args)))))
  ([f g & fs]
   (core.reduce comp (core.cons f (core.cons g fs)))))

(defn eq
  "Comparison function.

Accepts arbitrary amount of values, and does the deep comparison.  If
values implement `__eq` metamethod, tries to use it, by checking if
first value is equal to second value, and the second value is equal to
the first value.  If values are not equal and are tables does the deep
comparison.  Tables as keys are supported."
  ([] true)
  ([_] true)
  ([a b]
   (if (rawequal a b)
       true
       (and (= a b) (= b a))
       true
       (= :table (type a) (type b))
       (do (var (res count-a) (values true 0))
           (each [k v (pairs* a) :until (not res)]
             (set res (eq v (do (var (res done) (values nil nil))
                                (each [k* v (pairs* b) :until done]
                                  (when (eq k* k)
                                    (set (res done) (values v true))))
                                res)))
             (set count-a (+ count-a 1)))
           (when res
             (let [count-b (accumulate [res 0 _ _ (pairs* b)]
                             (+ res 1))]
               (set res (= count-a count-b))))
           res)
       false))
  ([a b & cs]
   (and (eq a b) (apply eq b cs))))

(set core.= eq)

(fn* core.not=
  "Same as (not (= obj1 fn*)core.)"
  ([_] false)
  ([x y] (not (eq x y)))
  ([x y & more]
   (not (apply eq x y more))))

(fn deep-index [tbl key]
  "This function uses the `eq` function to compare keys of the given
table `tbl` and the given `key`.  Several other functions also reuse
this indexing method, such as sets."
  (accumulate [res nil
               k v (pairs* tbl)
               :until res]
    (when (eq k key)
      v)))

(fn deep-newindex [tbl key val]
  "This function uses the `eq` function to compare keys of the given
table `tbl` and the given `key`. If the key is found it's being
set, if not a new key is set."
  (var done false)
  (when (= :table (type key))
    (each [k _ (pairs* tbl) :until done]
      (when (eq k key)
        (rawset tbl k val)
        (set done true))))
  (when (not done)
    (rawset tbl key val)))

(fn* core.memoize
  "Returns a memoized version of a referentially transparent function.
The memoized version of the function keeps a cache of the mapping from
arguments to results and, when calls with the same arguments are
repeated often, has higher performance at the expense of higher memory
use."
  [f]
  (let [memo (setmetatable {} {:__index deep-index})]
    (fn* [& args]
      (case (. memo args)
        res (unpack* res 1 res.n)
        _ (let [res (pack* (f ...))]
            (tset memo args res)
            (unpack* res 1 res.n))))))

(defn deref
  "Dereference an object."
  [x]
  (case (getmetatable x)
    {:cljlib/deref f} (f x)
    _ (error "object doesn't implement cljlib/deref metamethod" 2)))

(fn* core.empty
  "Get an empty variant of a given collection."
  [x]
  (case (getmetatable x)
    {:cljlib/empty f} (f)
    _ (case (type x)
        :table []
        :string ""
        _ (error (.. "don't know how to create empty variant of type " _)))))

(fn core.str [...]
  "With no args, returns the empty string. With one arg `x`, returns
`tostring(x)`. `(str nil)` returns the empty string. With more than one
arg, returns the concatenation of the `str` values of the args."
  (table.concat
   (fcollect [i 1 (select :# ...)]
     (case (select i ...)
       nil ""
       x (tostring x)))))

(fn core.not [x]
  "Returns `true` if `x` is logical `false`, `false` otherwise."
  (not x))

(fn core.partial [f ...]
  "Takes a function `f` and fewer than the normal arguments to `f`, and
returns a `fn` that takes a variable number of additional args. When
called, the returned function calls `f` with args + additional args."
  (let [args (#(doto [$...] (tset :n (select :# $...))) ...)]
    (fn [...]
      (let [args* []]
        (for [i 1 args.n]
          (tset args* i (. args i)))
        (for [i 1 (select :# ...)]
          (tset args* (+ args.n i) (select i ...)))
        (f ((or table.unpack _G.unpack) args*))))))

(fn* core.ex-info
  "Create an error object that carries a `map` of additional data."
  [message map]
  (setmetatable {: message : map} {:cljlib/type :ex-info
                     :__tostring #message :__fennelview #message}))

(fn* core.ex-data
  "Returns exception data (a map) if `ex` is an ExceptionInfo.
Otherwise returns `nil`."
  [ex]
  (case (getmetatable ex)
    {:cljlib/type :ex-info} ex.map))

(fn* core.ex-message
  "Returns the message attached to `ex` if ex is a ExceptionInfo.
Otherwise, if `ex` is a string, returns `ex`, otherwise returns nil."
  [ex]
  (case (getmetatable ex)
    {:cljlib/type :ex-info} ex.message
    _ (when (= :string (type ex)) ex)))

;;; IO

(fn core.format [fmt ...]
  "Formats a string `fmt` using `string.format`, see the Lua manual for format
  string syntax."
  {:fnl/arglist [fmt & args]}
  (string.format
   fmt
   ((or _G.unpack table.unpack)
    (fcollect [i 1 (select :# ...)]
      (if (= :table (type (select i ...)))
          (view (select i ...) {:one-line? true})
          (select i ...))))))

(fn* core.newline []
  "Prints a newline."
  (io.write "\n")
  nil)

(fn* core.flush []
  (io.flush)
  nil)

(fn core.print [...]
  "Prints the object(s) to the output stream that is the current value
of `io.output`.  `print` and `println` produce output for human consumption."
  {:fnl/arglist [& more]}
  (-> (fcollect [i 1 (select :# ...)]
        (if (= :table (type (select i ...)))
            (view (select i ...) {:one-line? true})
            (select i ...)))
      (table.concat " ")
      io.write)
  nil)

(fn core.print-str [...]
  "`print` to a string, returning it."
  {:fnl/arglist [& xs]}
  (with-out-str
    (core.print ...)))

(fn core.println [...]
  "Same as `print` followed by `(newline)`"
  {:fnl/arglist [& more]}
  (core.print ...)
  (core.newline))

(fn core.println-str [...]
  "`println` to a string, returning it."
  {:fnl/arglist [& xs]}
  (with-out-str
    (core.println ...)))

(fn core.printf [...]
  "Prints formatted output, as per `format`."
  {:fnl/arglist [fmt & args]}
  (core.print (core.format ...)))

(fn core.pr [...]
  "Prints the object(s) to the output.  Prints the object(s), separated
by spaces if there is more than one.  By default, `pr` and `prn` print
in a way that objects can be read by the reader."
  {:fnl/arglist ([] [x] [x & more])}
  (-> (fcollect [i 1 (select :# ...)]
        (view (select i ...) {:one-line? true}))
      (table.concat " ")
      io.write)
  nil)

(fn core.pr-str [...]
  "`pr` to a string, returning it."
  {:fnl/arglist [& xs]}
  (with-out-str
    (core.pr ...)))

(fn core.prn [...]
  "Same as `pr` followed by `(newline)`."
  {:fnl/arglist [& more]}
  (core.pr ...)
  (core.newline)
  nil)

(fn core.prn-str [...]
  "`prn` to a string, returning it."
  {:fnl/arglist [& xs]}
  (with-out-str
    (core.prn ...)))

;;; Tests and predicates

(defn nil?
  "Test if `x` is nil."
  ([] true)
  ([x] (= x nil)))

(fn* core.fnil
  "Takes a function `f`, and returns a function that calls `f`, replacing
a `nil` first argument to `f` with the supplied value `x`. Higher arity
versions can replace arguments in the second and third
positions (`y`, `z`). Note that the function `f` can take any number of
arguments, not just the one(s) being `nil`-patched."
  ([f x]
   (fn*
     ([a] (f (if (nil? a) x a)))
     ([a b] (f (if (nil? a) x a) b))
     ([a b c] (f (if (nil? a) x a) b c))
     ([a b c & ds] (apply f (if (nil? a) x a) b c ds))))
  ([f x y]
   (fn*
     ([a b] (f (if (nil? a) x a) (if (nil? b) y b)))
     ([a b c] (f (if (nil? a) x a) (if (nil? b) y b) c))
     ([a b c & ds] (apply f (if (nil? a) x a) (if (nil? b) y b) c ds))))
  ([f x y z]
   (fn*
     ([a b] (f (if (nil? a) x a) (if (nil? b) y b)))
     ([a b c] (f (if (nil? a) x a) (if (nil? b) y b) (if (nil? c) z c)))
     ([a b c & ds] (apply f (if (nil? a) x a) (if (nil? b) y b) (if (nil? c) z c) ds)))))

(defn fn?
  "Returns `true` if `x` is a function, i.e. is an object created via `fn`."
  [x]
  (= :function (type x)))

(defn ifn?
  "Returns true if x implements IFn. Note that many data structures
  (e.g. sets and maps) implement IFn"
  [x]
  (case (getmetatable x)
    {:__call f} (ifn? f)
    _ (fn? x)))

(fn* core.zero?
  "Test if `x` is equal to zero."
  [x]
  (= x 0))

(defn pos?
  "Test if `x` is greater than zero."
  [x]
  (> x 0))

(defn neg?
  "Test if `x` is less than zero."
  [x]
  (< x 0))

(defn even?
  "Test if `x` is even."
  [x]
  (= (% x 2) 0))

(fn* core.odd?
  "Test if `x` is odd."
  [x]
  (not (even? x)))

(defn string?
  "Test if `x` is a string."
  [x]
  (= (type x) :string))

(fn* core.char?
  "Return true if x is a Character"
  [s]
  (and (string? s) (= (length s) 1)))

(fn* core.boolean?
  "Test if `x` is a Boolean"
  [x]
  (= (type x) :boolean))

(fn* core.true?
  "Test if `x` is `true`"
  [x]
  (= x true))

(fn* core.false?
  "Test if `x` is `false`"
  [x]
  (= x false))

(defn int?
  "Test if `x` is a number without floating point data.

Number is rounded with `math.floor` and compared with original number."
  [x]
  (and (= (type x) :number)
       (= x (math.floor x))))

(fn* core.number?
  "Returns `true` if `x` is a number."
  [x]
  (= :number (type x)))

(fn* core.pos-int?
  "Test if `x` is a positive integer."
  [x]
  (and (int? x)
       (pos? x)))

(fn* core.neg-int?
  "Test if `x` is a negative integer."
  [x]
  (and (int? x)
       (neg? x)))

(fn* core.double?
  "Test if `x` is a number with floating point data."
  [x]
  (and (= (type x) :number)
       (not= x (math.floor x))))

(defn empty?
  "Check if collection is empty."
  [x]
  (case (type x)
    :table
    (case (getmetatable x)
      {:cljlib/type :seq}
      (nil? (core.seq x))
      (where (or nil {:cljlib/type nil}))
      (let [(next*) (pairs* x)]
        (= (next* x) nil)))
    :string (= x "")
    :nil true
    _ (error "empty?: unsupported collection")))

(fn* core.not-empty
  "If `x` is empty, returns `nil`, otherwise `x`."
  [x]
  (when (not (empty? x))
    x))

(defn map?
  "Check whether `x` is an associative table.

Non-empty tables are tested by calling `next`. If the length of the
table is greater than zero, the last integer key is passed to the
`next`, and if `next` returns a key, the table is considered
associative. If the length is zero, `next` is called with what `paris`
returns for the table, and if the key is returned, table is considered
associative.

Empty tables can't be analyzed with this method, and `map?` will
always return `false`.  If you need this test pass for empty table,
see `hash-map` for creating tables that have additional metadata
attached for this test to work.

# Examples
Non-empty map:

``` fennel
(assert-is (map? {:a 1 :b 2}))
```

Empty tables don't pass the test:

``` fennel
(assert-not (map? {}))
```

Empty tables created with `hash-map` will pass the test:

``` fennel
(assert-is (map? (hash-map)))
```"
  [x]
  (if (= :table (type x))
      (case (getmetatable x)
        {:cljlib/type :hash-map} true
        {:cljlib/type :sorted-map} true
        (where (or nil {:cljlib/type nil}))
        (let [len (length* x)
              (nxt t k) (pairs* x)]
          (not= nil (nxt t (if (= len 0) k len))))
        _ false)
      false))

(fn* core.associative?
  "Returns true if `coll` is associative."
  [coll]
  (map? coll))

(defn vector?
  "Check whether `tbl` is a sequential table.

Non-empty sequential tables are tested for two things:
- `next` returns the key-value pair,
- key, that is returned by the `next` is equal to `1`.

Empty tables can't be analyzed with this method, and `vector?` will
always return `false`.  If you need this test pass for empty table,
see `vector` for creating tables that have additional
metadata attached for this test to work.

# Examples
Non-empty vector:

``` fennel
(assert-is (vector? [1 2 3 4]))
```

Empty tables don't pass the test:

``` fennel
(assert-not (vector? []))
```

Empty tables created with `vector` will pass the test:

``` fennel
(assert-is (vector? (vector)))
```"
  [x]
  (if (= :table (type x))
      (case (getmetatable x)
        {:cljlib/type :vector} true
        (where (or nil {:cljlib/type nil}))
        (let [len (length* x)
              (nxt t k) (pairs* x)]
          (if (not= nil (nxt t (if (= len 0) k len))) false
              (> len 0) true
              false))
        _ false)
      false))

(fn* core.set?
  "Check if object is a set."
  [x]
  (case (getmetatable x)
    {:cljlib/type :hash-set} true
    _ false))

(defn seq?
  "Check if object is a sequence."
  [x]
  (lazy.seq? x))

(fn* core.some?
  "Returns true if x is not nil, false otherwise."
  [x]
  (not= x nil))

(fn* core.any?
  "Returns true given any argument."
  [_]
  true)

(fn* core.identical?
  "Tests if 2 arguments are the same object."
  [x y]
  (rawequal x y))

(fn* core.chunked-seq?
  "Returns `true` if sequence `s` is fn*.core."
  [_s]
  ;; lazy-seq doesn't implement chunking
  false)

(defn sequential?
  "Returns true if `coll` is sequential: when `seq?` or `vector?`
predicates return `true` for the given `coll`."
  [coll]
  (or (seq? coll) (vector? coll)))

;;; Vector

(fn vec->transient [immutable]
  (fn [vec]
    (var len (length vec))
    (->> {:__index (fn [_ i]
                     (if (<= i len)
                         (. vec i)))
          :__len #len
          :cljlib/type :transient
          :cljlib/conj #(error "can't `conj` onto transient vector, use `conj!`")
          :cljlib/assoc #(error "can't `assoc` onto transient vector, use `assoc!`")
          :cljlib/dissoc #(error "can't `dissoc` onto transient vector, use `dissoc!`")
          :cljlib/conj! (fn [tvec v]
                          (set len (+ len 1))
                          (doto tvec (tset len v)))
          :cljlib/assoc! (fn [tvec ...]
                           (let [len (length tvec)]
                             (for [i 1 (select "#" ...) 2]
                               (let [(_k v) (select i ...)]
                                 (if (<= 1 i len)
                                     (tset tvec i v)
                                     (error (.. "index " i " is out of bounds"))))))
                           tvec)
          :cljlib/pop! (fn [tvec]
                         (if (= len 0)
                             (error "transient vector is empty" 2)
                             (let [_val (table.remove tvec)]
                               (set len (- len 1))
                               tvec)))
          :cljlib/dissoc! #(error "can't `dissoc!` with a transient vector")
          :cljlib/persistent! (fn [tvec]
                                (let [v (fcollect [i 1 len] (. tvec i))]
                                  (while (> len 0)
                                    (table.remove tvec)
                                    (set len (- len 1)))
                                  (setmetatable tvec
                                                {:__index #(error "attempt to use transient after it was persistet")
                                                 :__newindex #(error "attempt to use transient after it was persistet")})
                                  (immutable (itable v))))}
         (setmetatable {}))))

(fn vec* [v len]
  (case (getmetatable v)
    mt (doto mt
         (tset :__len (constantly (or len (length* v))))
         (tset :cljlib/type :vector)
         (tset :cljlib/editable true)
         (tset :cljlib/conj
               (fn [t v]
                 (let [len (length* t)]
                   (vec* (itable.assoc t (+ len 1) v) (+ len 1)))))
         (tset :cljlib/pop
               (fn [t]
                 (let [len (- (length* t) 1)
                       coll []]
                   (when (< len 0)
                     (error "can't pop empty vector" 2))
                   (for [i 1 len]
                     (tset coll i (. t i)))
                   (vec* (itable coll) len))))
         (tset :cljlib/empty
               (fn [] (vec* (itable []))))
         (tset :cljlib/transient (vec->transient vec*))
         (tset :__fennelview (fn [coll view inspector indent]
                               (if (empty? coll)
                                   "[]"
                                   (let [lines (fcollect [i 1 (length* coll)]
                                                 (.. " " (view (. coll i) inspector indent)))]
                                     (tset lines 1 (.. "[" (string.gsub (or (. lines 1) "") "^%s+" "")))
                                     (tset lines (length lines) (.. (. lines (length lines)) "]"))
                                     lines)))))
    nil (vec* (setmetatable v {})))
  v)

(defn vec
  "Coerce collection `coll` to a vector."
  [coll]
  (cond (empty? coll) (vec* (itable []) 0)
        (vector? coll) (vec* (itable coll) (length* coll))
        :else (let [packed (-> coll core.seq lazy.pack)
                    len packed.n]
                (-> packed
                    (doto (tset :n nil))
                    (itable {:fast-index? true})
                    (vec* len)))))

(defn vector
  "Constructs sequential table out of its arguments.

Sets additional metadata for function `vector?` to work.

# Examples

``` fennel
(def :private v (vector 1 2 3 4))
(assert-eq v [1 2 3 4])
```"
  [& args]
  (vec args))

(defn nth
  "Returns the value at the `index`. `get` returns `nil` if `index` out
of bounds, `nth` raises an error unless `not-found` is supplied.
`nth` also works for strings and sequences."
  ([coll i]
   (if (vector? coll)
       (if (or (< i 1) (< (length* coll) i))
           (error (string.format "index %d is out of bounds" i))
           (. coll i))
       (string? coll)
       (nth (vec coll) i)
       (seq? coll)
       (nth (vec coll) i)
       :else
       (error "expected an indexed collection")))
  ([coll i not-found]
   (assert (int? i) "expected an integer key")
   (if (vector? coll)
       (or (. coll i) not-found)
       (string? coll)
       (nth (vec coll) i not-found)
       (seq? coll)
       (nth (vec coll) i not-found)
       :else
       (error "expected an indexed collection"))))

;;; Sequences

(defn- seq*
  "Add cljlib sequence meta-info."
  [x]
  (case (getmetatable x)
    mt (doto mt
         (tset :cljlib/type :seq)
         (tset :cljlib/conj
               (fn [s v] (core.cons v s)))
         (tset :cljlib/empty #(core.list))))
  x)

(defn seq
  "Construct a sequence from the given collection `coll`.  If `coll` is
an associative table, returns sequence of vectors with key and value.
If `col` is sequential table, returns its shallow copy.  If `col` is
string, return sequential table of its codepoints.

# Examples
Sequential tables are transformed to sequences:

``` fennel
(seq [1 2 3 4]) ;; @seq(1 2 3 4)
```

Associative tables are transformed to format like this `[[key1 value1]
... [keyN valueN]]` and order is non-deterministic:

``` fennel
(seq {:a 1 :b 2 :c 3}) ;; @seq([:b 2] [:a 1] [:c 3])
```"
  [coll]
  (seq* (case (getmetatable coll)
          {:cljlib/seq f} (f coll)
          _ (cond (lazy.seq? coll) (lazy.seq coll)
                  (map? coll) (lazy.map vec coll)
                  :else (lazy.seq coll)))))

(fn* core.rseq
  "Returns, in possibly-constant time, a seq of the items in `rev` in reverse order.
Input must be traversable with `ipairs`.  Doesn't work in constant
time if `rev` implements a linear-time `__len` metamethod, or invoking
Lua `#` operator on `rev` takes linar time.  If `t` is empty returns
`nil`.

# Examples

``` fennel
(def :private v [1 2 3])
(def :private r (rseq v))

(assert-eq (reverse v) r)
```"
  [rev]
  (seq* (lazy.rseq rev)))

(defn lazy-seq*
  "Create lazy sequence from the result of calling a function `f`.
Delays execution of `f` until sequence is consumed.  `f` must return a
sequence or a vector.  There's a convenience macro `lazy-seq'
automates the process of function creation."
  [f]
  (seq* (lazy.lazy-seq* f)))

(defn first
  "Return first element of a `coll`. Calls `seq` on its argument."
  [coll]
  (lazy.first (seq coll)))

(defn rest
  "Returns a sequence of all elements of a `coll` but the first one.
Calls `seq` on its argument."
  [coll]
  (seq* (lazy.rest (seq coll))))

(defn- next*
  "Return the tail of a sequence.

If the sequence is empty, returns nil."
  [s]
  (seq* (lazy.next s)))

(set core.next next*) ; luajit doesn't like next redefinition

(fn* core.second
  "Same as `(first (next x))`"
  [x]
  (first (next* x)))

(fn* core.ffirst
  "Same as (first (first x))"
  [x]
  (first (first x)))

(fn* core.nfirst
  "Same as (first (next x))"
  [x]
  (first (next* x)))

(fn* core.nnext
  "Same as (next (next x))"
  [x]
  (next* (next* x)))

(fn* core.instance?
  [_c _x]
  ;; TODO: implement
  false)

(fn* core.count
  "Count amount of elements in the sequence."
  [s]
  (case (getmetatable s)
    {:cljlib/type :vector} (length* s)
    _ (lazy.count s)))

(defn cons
  "Construct a cons cell.
Prepends new `head` to a `tail`, which must be either a table,
sequence, or nil.

# Examples

``` fennel
(assert-eq [0 1] (cons 0 [1]))
(assert-eq (list 0 1 2 3) (cons 0 (cons 1 (list 2 3))))
```"
  [head tail]
  (seq* (lazy.cons head tail)))

;; NOTE: `list` is implemented trhu `fn` because `defn` internaly uses `list`
(fn core.list
  [...]
  "Create eager sequence of provided values.

# Examples

``` fennel
(local l (list 1 2 3 4 5))
(assert-eq [1 2 3 4 5] l)
```"
  (seq* (lazy.list ...)))

(local list core.list)

(fn* core.list*
  "Creates a new sequence containing the items prepended to the rest,
the last of which will be treated as a sequence.

# Examples

``` fennel
(local l (list* 1 2 3 [4 5]))
(assert-eq [1 2 3 4 5] l)
```"
  [& args]
  (seq* (apply lazy.list* args)))

(defn last
  "Returns the last element of a `coll`. Calls `seq` on its argument."
  [coll]
  (case (next* coll)
    coll* (last coll*)
    _ (first coll)))

(fn* core.butlast
  "Returns everything but the last element of the `coll` as a new
  sequence.  Calls `seq` on its argument."
  [coll]
  (seq (lazy.drop-last coll)))

(defn map
  "Returns a lazy sequence consisting of the result of applying `f` to
the set of first items of each `coll`, followed by applying `f` to the
set of second items in each `coll`, until any one of the `colls` is
exhausted.  Any remaining items in other `colls` are ignored. Function
`f` should accept number-of-colls arguments. Returns a transducer when
no collection is provided.

# Examples

``` fennel
(map #(+ $ 1) [1 2 3]) ;; => @seq(2 3 4)
(map #(+ $1 $2) [1 2 3] [4 5 6]) ;; => @seq(5 7 9)
(def :private res (map #(+ $ 1) [:a :b :c])) ;; will raise an error only when realized
```"
  ([f]
   (fn* [rf]
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (rf result (f input)))
       ([result input & inputs]
        (rf result (apply f input inputs))))))
  ([f coll]
   (seq* (lazy.map f coll)))
  ([f coll & colls]
   (seq* (apply lazy.map f coll colls))))

(fn* core.mapv
  "Returns a vector consisting of the result of applying `f` to the
set of first items of each `coll`, followed by applying `f` to the set
of second items in each coll, until any one of the `colls` is
exhausted.  Any remaining items in other collections are ignored.
Function `f` should accept number-of-colls arguments."
  ([f coll]
   (->> coll
        (core.transduce (map f)
                        core.conj!
                        (core.transient (vector)))
        core.persistent!))
  ([f coll & colls] (vec (apply map f coll colls))))

(fn* core.map-indexed
  "Returns a lazy sequence consisting of the result of applying `f` to 1
and the first item of `coll`, followed by applying `f` to 2 and the
second item in `coll`, etc., until `coll` is exhausted.  Returns a
transducer when no collection is provided."
  ([f]
   (fn* [rf]
     (var i -1)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (set i (+ i 1))
        (rf result (f i input))))))
  ([f coll]
   (seq* (lazy.map-indexed f coll))))

(fn* core.mapcat
  "Apply `concat` to the result of calling `map` with `f` and
collections `colls`. Returns a transducer when no collection is
provided."
  ([f]
   (comp (map f) core.cat))
  ([f & colls]
   (seq* (apply lazy.mapcat f colls))))

(defn filter
  "Returns a lazy sequence of the items in `coll` for which
`pred` returns logical true. Returns a transducer when no collection
is provided."
  ([pred]
   (fn* [rf]
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (if (pred input)
            (rf result input)
            result)))))
  ([pred coll]
   (seq* (lazy.filter pred coll))))

(fn* core.filterv
  "Returns a vector of the items in `coll` for which
`pred` returns logical true."
  [pred coll]
  (vec (filter pred coll)))

(defn every?
  "Test if every item in `coll` satisfies the `pred`."
  [pred coll]
  (lazy.every? pred coll))

(fn* core.not-every?
  "Returns `false` if `(pred x)` is logical `true` for every `x` in
`coll`, else `true`."
  [pred coll]
  (not (every? pred coll)))

(defn some
  "Test if any item in `coll` satisfies the `pred`."
  [pred coll]
  (lazy.some? pred coll))

(fn* core.not-any?
  "Test if no item in `coll` satisfy the `pred`."
  [pred coll]
  (some #(not (pred $)) coll))

(defn range
  "Returns lazy sequence of numbers from `lower` to `upper` with optional
`step`."
  ([] (seq* (lazy.range)))
  ([upper] (seq* (lazy.range upper)))
  ([lower upper] (seq* (lazy.range lower upper)))
  ([lower upper step] (seq* (lazy.range lower upper step))))

(defn concat
  "Return a lazy sequence of concatenated `colls`."
  [& colls]
  (seq* (apply lazy.concat colls)))

(fn* core.reverse
  "Returns a lazy sequence with same items as in `coll` but in reverse order."
  [coll]
  (seq* (lazy.reverse coll)))

(fn* core.take
  "Returns a lazy sequence of the first `n` items in `coll`, or all items if
there are fewer than `n`."
  ([n]
   (fn* [rf]
     (var n n)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (let [result (if (< 0 n)
                         (rf result input)
                         result)]
          (set n (- n 1))
          (if (not (< 0 n))
              (core.ensure-reduced result)
              result))))))
  ([n coll]
   (seq* (lazy.take n coll))))

(fn* core.take-while
  "Take the elements from the collection `coll` until `pred` returns logical
false for any of the elemnts.  Returns a lazy sequence. Returns a
transducer when no collection is provided."
  ([pred]
   (fn* [rf]
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (if (pred input)
            (rf result input)
            (core.reduced result))))))
  ([pred coll]
   (seq* (lazy.take-while pred coll))))

(defn drop
  "Drop `n` elements from collection `coll`, returning a lazy sequence
of remaining elements. Returns a transducer when no collection is
provided."
  ([n]
   (fn* [rf]
     (var nv n)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (let [n nv]
          (set nv (- nv 1))
          (if (pos? n)
              result
              (rf result input)))))))
  ([n coll]
   (seq* (lazy.drop n coll))))

(fn* core.drop-while
  "Drop the elements from the collection `coll` until `pred` returns logical
false for any of the elemnts.  Returns a lazy sequence. Returns a
transducer when no collection is provided."
  ([pred]
   (fn* [rf]
     (var dv true)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (let [drop? dv]
          (if (and drop? (pred input))
              result
              (do
                (set dv nil)
                (rf result input))))))))
  ([pred coll]
   (seq* (lazy.drop-while pred coll))))

(fn* core.drop-last
  "Return a lazy sequence from `coll` without last `n` elements."
  ([] (seq* (lazy.drop-last)))
  ([coll] (seq* (lazy.drop-last coll)))
  ([n coll] (seq* (lazy.drop-last n coll))))

(fn* core.take-last
  "Return a sequence of last `n` elements of the `coll`."
  [n coll]
  (seq* (lazy.take-last n coll)))

(fn* core.take-nth
  "Return a lazy sequence of every `n` item in `coll`. Returns a
transducer when no collection is provided."
  ([n]
   (fn* [rf]
     (var iv -1)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (set iv (+ iv 1))
        (if (= 0 (% iv n))
            (rf result input)
            result)))))
  ([n coll]
   (seq* (lazy.take-nth n coll))))

(fn* core.split-at
  "Return a table with sequence `coll` being split at `n`"
  [n coll]
  (vec (lazy.split-at n coll)))

(fn* core.split-with
  "Return a table with sequence `coll` being split with `pred`"
  [pred coll]
  (vec (lazy.split-with pred coll)))

(fn* core.nthrest
  "Returns the nth rest of `coll`, `coll` when `n` is 0.

# Examples

``` fennel
(assert-eq (nthrest [1 2 3 4] 3) [4])
(assert-eq (nthrest [1 2 3 4] 2) [3 4])
(assert-eq (nthrest [1 2 3 4] 1) [2 3 4])
(assert-eq (nthrest [1 2 3 4] 0) [1 2 3 4])
```
"
  [coll n]
  (seq* (lazy.nthrest coll n)))

(fn* core.nthnext
  "Returns the nth next of `coll`, (seq coll) when `n` is 0."
  [coll n]
  (lazy.nthnext coll n))

(fn* core.keep
  "Returns a lazy sequence of the non-nil results of calling `f` on the
items of the `coll`. Returns a transducer when no collection is
provided."
  ([f]
   (fn* [rf]
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (let [v (f input)]
          (if (nil? v)
              result
              (rf result v)))))))
  ([f coll]
   (seq* (lazy.keep f coll))))

(fn* core.keep-indexed
  "Returns a lazy sequence of the non-nil results of (f index item) in
the `coll`.  Note, this means false return values will be included.
`f` must be free of side effects. Returns a transducer when no
collection is provided."
  ([f]
   (fn* [rf]
     (var iv -1)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (set iv (+ iv 1))
        (let [v (f iv input)]
          (if (nil? v)
              result
              (rf result v)))))))
  ([f coll]
   (seq* (lazy.keep-indexed f coll))))

(fn* core.partition
  "Given a collection `coll`, returns a lazy sequence of lists of `n`
items each, at offsets `step` apart. If `step` is not supplied,
defaults to `n`, i.e. the partitions do not overlap. If a `pad`
collection is supplied, use its elements as necessary to complete last
partition up to `n` items. In case there are not enough padding
elements, return a partition with less than `n` items."
  ([n coll] (map seq* (lazy.partition n coll)))
  ([n step coll] (map seq* (lazy.partition n step coll)))
  ([n step pad coll] (map seq* (lazy.partition n step pad coll))))

(fn array []
  (var len 0)
  (->> {:__len #len
        :__index {:clear (fn [self]
                           (while (not= 0 len)
                             (tset self len nil)
                             (set len (- len 1))
                             self))
                  :add (fn [self val]
                         (set len (+ len 1))
                         (tset self len val)
                         self)}}
       (setmetatable [])))

(fn* core.partition-by
  "Applies `f` to each value in `coll`, splitting it each time `f`
returns a new value.  Returns a lazy seq of partitions.  Returns a
transducer, if collection is not supplied."
  ([f]
   (fn* [rf]
     (let [a (array)
           none {}]
       (var pv none)
       (fn*
         ([] (rf))
         ([result]
          (rf (if (empty? a)
                  result
                  (let [v (vec a)]
                    (a:clear)
                    (core.unreduced (rf result v))))))
         ([result input]
          (let [pval pv
                val (f input)]
            (set pv val)
            (if (or (= pval none)
                    (= val pval))
                (do
                  (a:add input)
                  result)
                (let [v (vec a)]
                  (a:clear)
                  (let [ret (rf result v)]
                    (when (not (core.reduced? ret))
                      (a:add input))
                    ret)))))))))
  ([f coll]
   (map seq* (lazy.partition-by f coll))))

(fn* core.partition-all
  "Given a collection `coll`, returns a lazy sequence of lists like
`partition`, but may include partitions with fewer than n items at the
end. Accepts addiitonal `step` argument, similarly to `partition`.
Returns a transducer, if collection is not supplied."
  ([n]
   (fn* [rf]
     (let [a (array)]
       (fn*
         ([] (rf))
         ([result]
          (rf (if (= 0 (length a))
                  result
                  (let [v (vec a)]
                    (a:clear)
                    (core.unreduced (rf result v))))))
         ([result input]
          (a:add input)
          (if (= n (length a))
              (let [v (vec a)]
                (a:clear)
                (rf result v))
              result))))))
  ([n coll]
   (map seq* (lazy.partition-all n coll)))
  ([n step coll]
   (map seq* (lazy.partition-all n step coll))))

(fn* core.reductions
  "Returns a lazy seq of the intermediate values of the reduction (as
per reduce) of `coll` by `f`, starting with `init`."
  ([f coll] (seq* (lazy.reductions f coll)))
  ([f init coll] (seq* (lazy.reductions f init coll))))

(defn contains?
  "Test if `elt` is in the `coll`.  It may be a linear search depending
on the type of the collection."
  [coll elt]
  (lazy.contains? coll elt))

(fn* core.distinct
  "Returns a lazy sequence of the elements of the `coll` without
duplicates.  Comparison is done by equality. Returns a transducer when
no collection is provided."
  ([]
   (fn* [rf]
     (let [seen (setmetatable {} {:__index deep-index})]
       (fn*
         ([] (rf))
         ([result] (rf result))
         ([result input]
          (if (. seen input)
              result
              (do
                (tset seen input true)
                (rf result input))))))))
  ([coll]
   (seq* (lazy.distinct coll))))

(defn dedupe
  "Returns a lazy sequence removing consecutive duplicates in coll.
Returns a transducer when no collection is provided."
  ([]
   (fn* [rf]
     (let [none {}]
       (var pv none)
       (fn*
         ([] (rf))
         ([result] (rf result))
         ([result input]
          (let [prior pv]
            (set pv input)
            (if (= prior input)
                result
                (rf result input))))))))
  ([coll] (core.sequence (dedupe) coll)))

(fn* core.random-sample
  "Returns items from `coll` with random probability of `prob` (0.0 -
1.0).  Returns a transducer when no collection is provided."
  ([prob]
   (filter (fn [] (< (math.random) prob))))
  ([prob coll]
   (filter (fn [] (< (math.random) prob)) coll)))

(fn* core.doall
  "Realize whole lazy sequence `seq`.

Walks whole sequence, realizing each cell.  Use at your own risk on
infinite sequences."
  [seq]
  (seq* (lazy.doall seq)))

(fn* core.dorun
  "Realize whole sequence `seq` for side effects.

Walks whole sequence, realizing each cell.  Use at your own risk on
infinite sequences."
  [seq]
  (lazy.dorun seq))

(fn* core.line-seq
  "Accepts a `file` handle, and creates a lazy sequence of lines using
`lines` metamethod.

# Examples

Lazy sequence of file lines may seem similar to an iterator over a
file, but the main difference is that sequence can be shared onve
realized, and iterator can't.  Lazy sequence can be consumed in
iterator style with the `doseq` macro.

Bear in mind, that since the sequence is lazy it should be realized or
truncated before the file is closed:

``` fennel
(let [lines (with-open [f (io.open \"deps.fnl\" :r)]
              (line-seq f))]
  ;; this will error because only first line was realized, but the
  ;; file was closed before the rest of lines were cached
  (assert-not (pcall next lines)))
```

Sequence is realized with `doall` before file was closed and can be shared:

``` fennel
(let [lines (with-open [f (io.open \"deps.fnl\" :r)]
              (doall (line-seq f)))]
  (assert-is (pcall next lines)))
```

Infinite files can't be fully realized, but can be partially realized
with `take`:

``` fennel
(let [lines (with-open [f (io.open \"/dev/urandom\" :r)]
              (doall (take 3 (line-seq f))))]
  (assert-is (pcall next lines)))
```"
  [file]
  (seq* (lazy.line-seq file)))

(fn* core.iterate
  "Returns an infinete lazy sequence of x, (f x), (f (f x)) etc."
  [f x]
  (seq* (lazy.iterate f x)))

(fn* core.remove
  "Returns a lazy sequence of the items in the `coll` without elements
for wich `pred` returns logical true. Returns a transducer when no
collection is provided."
  ([pred]
   (filter (complement pred)))
  ([pred coll]
   (seq* (lazy.remove pred coll))))

(fn* core.cycle
  "Create a lazy infinite sequence of repetitions of the items in the
`coll`."
  [coll]
  (seq* (lazy.cycle coll)))

(fn* core.repeat
  "Takes a value `x` and returns an infinite lazy sequence of this value.

# Examples

``` fennel
(assert-eq 20 (reduce add (take 10 (repeat 2))))
```"
  [x]
  (seq* (lazy.repeat x)))

(fn* core.repeatedly
  "Takes a function `f` and returns an infinite lazy sequence of
function applications.  Rest arguments are passed to the function."
  [f & args]
  (seq* (apply lazy.repeatedly f args)))

(defn tree-seq
  "Returns a lazy sequence of the nodes in a tree, via a depth-first walk.

`branch?` must be a function of one arg that returns true if passed a
node that can have children (but may not).  `children` must be a
function of one arg that returns a sequence of the children.  Will
only be called on nodes for which `branch?` returns true.  `root` is
the root node of the tree.

# Examples

For the given tree `[\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]`:

        A
       / \\
      B   C
     / \\   \\
    D   E   F

Calling `tree-seq` with `next` as the `branch?` and `rest` as the
`children` returns a flat representation of a tree:

``` fennel
(assert-eq (map first (tree-seq next rest [\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]))
           [\"A\" \"B\" \"D\" \"E\" \"C\" \"F\"])
```"
  [branch? children root]
  (seq* (lazy.tree-seq branch? children root)))

(fn* core.flatten
  "Takes collection `coll` containing any nested combination of
sequential things (lists, vectors, etc.) and returns their contents as
a single, flat lazy sequence.  `(flatten nil)` returns an empty
sequence."
  [coll]
  (filter (complement sequential?)
          (rest (tree-seq sequential? seq coll))))

(fn* core.interleave
  "Returns a lazy sequence of the first item in each sequence, then the
second one, until any sequence exhausts."
  ([] (seq* (lazy.interleave)))
  ([s] (seq* (lazy.interleave s)))
  ([s1 s2] (seq* (lazy.interleave s1 s2)))
  ([s1 s2 & ss] (seq* (apply lazy.interleave s1 s2 ss))))

(fn* core.interpose
  "Returns a lazy sequence of the elements of `coll` separated by
`separator`. Returns a transducer when no collection is provided."
  ([sep]
   (fn* [rf]
     (var started false)
     (fn*
       ([] (rf))
       ([result] (rf result))
       ([result input]
        (if started
            (let [sepr (rf result sep)]
              (if (core.reduced? sepr)
                  sepr
                  (rf sepr input)))
            (do
              (set started true)
              (rf result input)))))))
  ([separator coll]
   (seq* (lazy.interpose separator coll))))

(defn halt-when
  "Returns a transducer that ends transduction when `pred` returns `true`
for an input. When `retf` is supplied it must be a `fn` of 2 arguments
- it will be passed the (completed) result so far and the input that
triggered the predicate, and its return value (if it does not throw an
exception) will be the return value of the transducer. If `retf` is
not supplied, the input that triggered the predicate will be
returned. If the predicate never returns `true` the transduction is
unaffected."
  ([pred]
   (halt-when pred nil))
  ([pred retf]
   (fn* [rf]
     (let [halt (setmetatable {} {:__fennelview #"#<halt>"})]
       (fn*
         ([] (rf))
         ([result]
          (if (and (map? result) (contains? result halt))
              result.value
              (rf result)))
         ([result input]
          (if (pred input)
              (core.reduced {halt true :value (if retf (retf (rf result) input) input)})
              (rf result input))))))))

(fn* core.realized?
  "Check if sequence's first element is realized."
  [s]
  (lazy.realized? s))

(fn* core.keys
  "Returns a sequence of the map's keys, in the same order as `seq`."
  [coll]
  (assert (or (map? coll) (empty? coll)) "expected a map")
  (if (empty? coll)
      (lazy.list)
      (lazy.keys coll)))

(fn* core.vals
  "Returns a sequence of the table's values, in the same order as `seq`."
  [coll]
  (assert (or (map? coll) (empty? coll)) "expected a map")
  (if (empty? coll)
      (lazy.list)
      (lazy.vals coll)))

(defn find
  "Returns the map entry for `key`, or `nil` if key is not present in
`coll`."
  [coll key]
  (assert (or (map? coll) (empty? coll)) "expected a map")
  (case (. coll key)
    v [key v]))

(fn* core.sort
  "Returns a sorted sequence of the items in `coll`. If no `comparator`
is supplied, uses `<`."
  ([coll]
   (case (seq coll)
     s (seq (itable.sort (vec s)))
     _ (list)))
  ([comparator coll]
   (case (seq coll)
     s (seq (itable.sort (vec s) comparator))
     _ (list))))

;;; Reduce

(defn reduce
  "`f` should be a function of 2 arguments. If `val` is not supplied,
returns the result of applying `f` to the first 2 items in `coll`,
then applying `f` to that result and the 3rd item, etc. If `coll`
contains no items, f must accept no arguments as well, and reduce
returns the result of calling `f` with no arguments.  If `coll` has
only 1 item, it is returned and `f` is not called.  If `val` is
supplied, returns the result of applying `f` to `val` and the first
item in `coll`, then applying `f` to that result and the 2nd item,
etc. If `coll` contains no items, returns `val` and `f` is not
called. Early termination is supported via `reduced`.

# Examples

``` fennel
(defn- add
  ([] 0)
  ([a] a)
  ([a b] (+ a b))
  ([a b & cs] (apply add (+ a b) cs)))
;; no initial value
(assert-eq 10 (reduce add [1 2 3 4]))
;; initial value
(assert-eq 10 (reduce add 1 [2 3 4]))
;; empty collection - function is called with 0 args
(assert-eq 0 (reduce add []))
(assert-eq 10.3 (reduce math.floor 10.3 []))
;; collection with a single element doesn't call a function unless the
;; initial value is supplied
(assert-eq 10.3 (reduce math.floor [10.3]))
(assert-eq 7 (reduce add 3 [4]))
```"
  ([f coll] (lazy.reduce f (seq coll)))
  ([f val coll] (lazy.reduce f val (seq coll))))

(defn reduced
  "Terminates the `reduce` early with a given `value`.

# Examples

``` fennel
(assert-eq :NaN
           (reduce (fn [acc x]
                     (if (not= :number (type x))
                         (reduced :NaN)
                         (+ acc x)))
                   [1 2 :3 4 5]))
```"
  [value]
  (doto (rdc.reduced value)
    (-> getmetatable (tset :cljlib/deref #($:unbox)))))

(defn reduced?
  "Returns true if `x` is the result of a call to reduced"
  [x]
  (rdc.reduced? x))

(fn* core.ensure-reduced
  "If `x` is already `reduced?`, returns it, else returns `(reduced x)`."
  [x]
  (if (reduced? x) x (reduced x)))

(fn* core.unreduced
  "If `x` is `reduced?`, returns `(deref x)`, else returns `x`."
  [x]
  (if (reduced? x) (deref x) x))

(fn* core.ensure-reduced
  "If x is already reduced?, returns it, else returns (reduced x)"
  [x]
  (if (reduced? x)
      x
      (reduced x)))

(defn- preserving-reduced [rf]
  (fn* [a b]
    (let [ret (rf a b)]
      (if (reduced? ret)
          (reduced ret)
          ret))))

(fn* core.cat
  "A transducer which concatenates the contents of each input, which must be a
  collection, into the reduction. Accepts the reducing function `rf`."
  [rf]
  (let [rrf (preserving-reduced rf)]
    (fn*
      ([] (rf))
      ([result] (rf result))
      ([result input]
       (reduce rrf result input)))))

(fn* core.reduce-kv
  "Reduces an associative table using function `f` and initial value `val`.

`f` should be a function of 3 arguments.  Returns the result of
applying `f` to `val`, the first key and the first value in `tbl`,
then applying `f` to that result and the 2nd key and value, etc.  If
`tbl` contains no entries, returns `val` and `f` is not called.  Note
that `reduce-kv` is supported on sequential tables and strings, where
the keys will be the ordinals.

Early termination is possible with the use of `reduced`
function.

# Examples
Reduce associative table by adding values from all keys:

``` fennel
(local t {:a1 1
          :b1 2
          :a2 2
          :b2 3})

(reduce-kv #(+ $1 $3) 0 t)
;; => 8
```

Reduce table by adding values from keys that start with letter `a`:

``` fennel
(local t {:a1 1
          :b1 2
          :a2 2
          :b2 3})

(reduce-kv (fn [res k v] (if (= (string.sub k 1 1) :a) (+ res v) res))
           0 t)
;; => 3
```"
  [f val s]
  (if (map? s)
      (reduce (fn [res [k v]] (f res k v)) val (seq s))
      (reduce (fn [res [k v]] (f res k v)) val (map vector (drop 1 (range)) (seq s)))))

(defn completing
  "Takes a reducing function `f` of 2 args and returns a function
suitable for transduce by adding an arity-1 signature that calls
`cf` (default - `identity`) on the result argument."
  ([f] (completing f identity))
  ([f cf]
   (fn*
     ([] (f))
     ([x] (cf x))
     ([x y] (f x y)))))

(defn transduce
  "`reduce` with a transformation of `f` (`xform`). If `init` is not
supplied, `f` will be called to produce it. `f` should be a reducing
step function that accepts both 1 and 2 arguments, if it accepts only
2 you can add the arity-1 with `completing`. Returns the result of
applying (the transformed) `xform` to `init` and the first item in
`coll`, then applying `xform` to that result and the 2nd item, etc. If
`coll` contains no items, returns `init` and `f` is not called. Note
that certain transforms may inject or skip items."
  ([xform f coll] (transduce xform f (f) coll))
  ([xform f init coll]
   (let [f (xform f)]
     (f (reduce f init (seq coll))))))

(fn* core.sequence
  "Coerces coll to a (possibly empty) sequence, if it is not already
one. Will not force a lazy seq. `(sequence nil)` yields an empty list,
When a transducer `xform` is supplied, returns a lazy sequence of
applications of the transform to the items in `coll`, i.e. to the set
of first items of each `coll`, followed by the set of second items in
each `coll`, until any one of the `colls` is exhausted.  Any remaining
items in other `colls` are ignored. The transform should accept
number-of-colls arguments"
  ([coll]
   (if (seq? coll) coll
       (or (seq coll) (list))))
  ([xform coll]
   (let [f (xform (completing #(cons $2 $1)))]
     (or ((fn step [coll]
            (if-some [s (seq coll)]
              (let [res (f nil (first s))]
                (cond (reduced? res) (f (deref res))
                      (seq? res) (concat res (lazy-seq* #(step (rest s))))
                      :else (step (rest s))))
              (f nil)))
          coll)
         (list))))
  ([xform coll & colls]
   (let [f (xform (completing #(cons $2 $1)))]
     (or ((fn step [colls]
            (if (every? seq colls)
                (let [res (apply f nil (map first colls))]
                  (cond (reduced? res) (f (deref res))
                        (seq? res) (concat res (lazy-seq* #(step (map rest colls))))
                        :else (step (map rest colls))))
                (f nil)))
          (cons coll colls))
         (list)))))

;;; Hash map

(fn map->transient [immutable]
  (fn [map]
    (let [removed (setmetatable {} {:__index deep-index})]
      (->> {:__index (fn [_ k]
                       (if (not (. removed k))
                           (. map k)))
            :cljlib/type :transient
            :cljlib/conj #(error "can't `conj` onto transient map, use `conj!`")
            :cljlib/assoc #(error "can't `assoc` onto transient map, use `assoc!`")
            :cljlib/dissoc #(error "can't `dissoc` onto transient map, use `dissoc!`")
            :cljlib/conj! (fn [tmap [k v]]
                            (if (= nil v)
                                (tset removed k true)
                                (tset removed k nil))
                            (doto tmap (tset k v)))
            :cljlib/assoc! (fn [tmap ...]
                             (for [i 1 (select "#" ...) 2]
                               (let [(k v) (select i ...)]
                                 (tset tmap k v)
                                 (if (= nil v)
                                     (tset removed k true)
                                     (tset removed k nil))))
                             tmap)
            :cljlib/dissoc! (fn [tmap ...]
                              (for [i 1 (select "#" ...)]
                                (let [k (select i ...)]
                                  (tset tmap k nil)
                                  (tset removed k true)))
                              tmap)
            :cljlib/persistent! (fn [tmap]
                                  (let [t (collect [k v (pairs tmap)
                                                    :into (collect [k v (pairs map)]
                                                            (values k v))]
                                            (values k v))]
                                    (each [k (pairs removed)]
                                      (tset t k nil))
                                    (each [_ k (ipairs (icollect [k (pairs* tmap)] k))]
                                      (tset tmap k nil))
                                    (setmetatable tmap
                                                  {:__index #(error "attempt to use transient after it was persistet")
                                                   :__newindex #(error "attempt to use transient after it was persistet")})
                                    (immutable (itable t))))}
           (setmetatable {})))))

(fn hash-map* [x]
  "Add cljlib hash-map meta-info."
  (case (getmetatable x)
    mt (doto mt
         (tset :cljlib/type :hash-map)
         (tset :cljlib/editable true)
         (tset :cljlib/conj
               (fn [t [k v] ...]
                 (apply core.assoc
                        t k v
                        (accumulate [kvs [] _ [k v] (ipairs* [...])]
                          (doto kvs
                            (table.insert k)
                            (table.insert v))))))
         (tset :cljlib/transient (map->transient hash-map*))
         (tset :cljlib/empty #(hash-map* (itable {}))))
    _ (hash-map* (setmetatable x {})))
  x)

(defn assoc
  "Associate `val` under a `key`.
Accepts extra keys and values.

# Examples

``` fennel
(assert-eq {:a 1 :b 2} (assoc {:a 1} :b 2))
(assert-eq {:a 1 :b 2} (assoc {:a 1 :b 1} :b 2))
(assert-eq {:a 1 :b 2 :c 3} (assoc {:a 1 :b 1} :b 2 :c 3))
```"
  ([tbl k v]
   (assert (or (nil? tbl) (map? tbl) (empty? tbl)) "expected a map")
   (assert (not (nil? k)) "attempt to use nil as key")
   (hash-map* (itable.assoc (or tbl {}) k v)))
  ([tbl k v & kvs]
   (assert (or (nil? tbl) (map? tbl) (empty? tbl)) "expected a map")
   (assert (not (nil? k)) "attempt to use nil as key")
   (hash-map* (apply itable.assoc (or tbl {}) k v kvs))))

(fn* core.assoc-in
  "Associate `val` into set of immutable nested tables `t`, via given `key-seq`.
Returns a new immutable table.  Returns a new immutable table.

# Examples

Replace value under nested keys:

``` fennel
(assert-eq
 {:a {:b {:c 1}}}
 (assoc-in {:a {:b {:c 0}}} [:a :b :c] 1))
```

Create new entries as you go:

``` fennel
(assert-eq
 {:a {:b {:c 1}} :e 2}
 (assoc-in {:e 2} [:a :b :c] 1))
```"
  [tbl key-seq val]
  (assert (or (nil? tbl) (map? tbl) (empty? tbl)) "expected a map or nil")
  (hash-map* (itable.assoc-in tbl key-seq val)))

(fn* core.update
  "Update table value stored under `key` by calling a function `f` on
that value. `f` must take one argument, which will be a value stored
under the key in the table.

# Examples

Same as `assoc` but accepts function to produce new value based on key value.

``` fennel
(assert-eq
 {:data \"THIS SHOULD BE UPPERCASE\"}
 (update {:data \"this should be uppercase\"} :data string.upper))
```"
  [tbl key f]
  (assert (or (nil? tbl) (map? tbl) (empty? tbl)) "expected a map")
  (hash-map* (itable.update tbl key f)))


(fn* core.update-in
  "Update table value stored under set of immutable nested tables, via
given `key-seq` by calling a function `f` on the value stored under the
last key.  `f` must take one argument, which will be a value stored
under the key in the table.  Returns a new immutable table.

# Examples

Same as `assoc-in` but accepts function to produce new value based on key value.

``` fennel
(fn capitalize-words [s]
  (pick-values 1
    (s:gsub \"(%a)([%w_`]*)\" #(.. ($1:upper) ($2:lower)))))

(assert-eq
 {:user {:name \"John Doe\"}}
 (update-in {:user {:name \"john doe\"}} [:user :name] capitalize-words))
```"
  [tbl key-seq f]
  (assert (or (nil? tbl) (map? tbl) (empty? tbl)) "expected a map or nil")
  (hash-map* (itable.update-in tbl key-seq f)))

(defn hash-map
  "Create associative table from `kvs` represented as sequence of keys
and values"
  ([]
   (hash-map* (itable {})))
  ([& kvs]
   (apply assoc {} kvs)))

(defn get
  "Get value from the table by accessing it with a `key`.
Accepts additional `not-found` as a marker to return if value wasn't
found in the table."
  ([tbl key] (get tbl key nil))
  ([tbl key not-found]
   (assert (or (map? tbl) (empty? tbl)) "expected a map")
   (or (. tbl key) not-found)))

(defn get-in
  "Get value from nested set of tables by providing key sequence.
Accepts additional `not-found` as a marker to return if value wasn't
found in the table."
  ([tbl keys] (get-in tbl keys nil))
  ([tbl keys not-found]
   (assert (or (map? tbl) (empty? tbl)) "expected a map")
   (var (res t done) (values tbl tbl nil))
   (each [_ k (ipairs* keys) :until done]
     (case (. t k)
       v (set (res t) (values v v))
       _ (set (res done) (values not-found true))))
   res))

(defn dissoc
  "Remove `key` from table `tbl`.  Optionally takes more `keys`."
  ([tbl] tbl)
  ([tbl key]
   (assert (or (map? tbl) (empty? tbl)) "expected a map")
   (hash-map* (doto tbl (tset key nil))))
  ([tbl key & keys]
   (apply dissoc (dissoc tbl key) keys)))

(fn* core.merge
  "Merge `maps` rght to left into a single hash-map."
  [& maps]
  (when (some identity maps)
    (->> maps
         (reduce (fn [a b] (collect [k v (pairs* b) :into a]
                             (values k v)))
                 {})
         itable
         hash-map*)))

(fn* core.frequencies
  "Return a table of unique entries from table `t` associated to amount
of their appearances.

# Examples

Count each entry of a random letter:

``` fennel
(let [fruits [:banana :banana :apple :strawberry :apple :banana]]
  (assert-eq (frequencies fruits)
             {:banana 3
              :apple 2
              :strawberry 1}))
```"
  [t]
  (hash-map* (itable.frequencies t)))

(fn* core.group-by
  "Group table items in an associative table under the keys that are
results of calling `f` on each element of sequential table `t`.
Elements that the function call resulted in `nil` returned in a
separate table.

# Examples

Group rows by their date:

``` fennel
(local rows
  [{:date \"2007-03-03\" :product \"pineapple\"}
   {:date \"2007-03-04\" :product \"pizza\"}
   {:date \"2007-03-04\" :product \"pineapple pizza\"}
   {:date \"2007-03-05\" :product \"bananas\"}])

(assert-eq (group-by #(. $ :date) rows)
           {\"2007-03-03\"
            [{:date \"2007-03-03\" :product \"pineapple\"}]
            \"2007-03-04\"
            [{:date \"2007-03-04\" :product \"pizza\"}
             {:date \"2007-03-04\" :product \"pineapple pizza\"}]
            \"2007-03-05\"
            [{:date \"2007-03-05\" :product \"bananas\"}]})
```"
  [f t]
  (hash-map* (pick-values 1 (itable.group-by f t))))

(fn* core.zipmap
  "Return an associative table with the `keys` mapped to the
corresponding `vals`."
  [keys vals]
  (hash-map* (itable (lazy.zipmap keys vals))))

(fn* core.replace
  "Given a map of replacement pairs and a vector/collection `coll`,
returns a vector/seq with any elements `=` a key in `smap` replaced
with the corresponding `val` in `smap`.  Returns a transducer when no
collection is provided."
  ([smap]
   (map #(if-let [e (find smap $)] (. e 2) $)))
  ([smap coll]
   (if (vector? coll)
       (->> coll
            (reduce (fn [res v]
                      (if-let [e (find smap v)]
                        (doto res (table.insert (. e 2)))
                        (doto res (table.insert v))))
                    [])
            itable
            vec*)
       (map #(if-let [e (find smap $)] (. e 2) $) coll))))

;;; Conj

(defn conj
  "Insert `x` as a last element of a table `tbl`.

If `tbl` is a sequential table or empty table, inserts `x` and
optional `xs` as final element in the table.

If `tbl` is an associative table, that satisfies `map?` test,
insert `[key value]` pair into the table.

Mutates `tbl`.

# Examples
Adding to sequential tables:

``` fennel
(conj [] 1 2 3 4)
;; => [1 2 3 4]
(conj [1 2 3] 4 5)
;; => [1 2 3 4 5]
```

Adding to associative tables:

``` fennel
(conj {:a 1} [:b 2] [:c 3])
;; => {:a 1 :b 2 :c 3}
```

Note, that passing literal empty associative table `{}` will not work:

``` fennel
(conj {} [:a 1] [:b 2])
;; => [[:a 1] [:b 2]]
(conj (hash-map) [:a 1] [:b 2])
;; => {:a 1 :b 2}
```

See `hash-map` for creating empty associative tables."
  ([] (vector))
  ([s] s)
  ([s x]
   (case (getmetatable s)
     {:cljlib/conj f} (f s x)
     _ (if (vector? s) (vec* (itable.insert s x))
           (map? s) (apply assoc s x)
           (nil? s) (cons x s)
           (empty? s) (vector x)
           (error "expected collection, got" (type s)))))
  ([s x & xs]
   (apply conj (conj s x) xs)))

(fn* core.disj
  "Returns a new set type, that does not contain the
specified `key` or `keys`."
  ([Set] Set)
  ([Set key]
   (case (getmetatable Set)
     {:cljlib/type :hash-set :cljlib/disj f} (f Set key)
     _ (error (.. "disj is not supported on " (class Set)) 2)))
  ([Set key & keys]
   (case (getmetatable Set)
     {:cljlib/type :hash-set :cljlib/disj f} (apply f Set key keys)
     _ (error (.. "disj is not supported on " (class Set)) 2))))

(fn* core.pop
  "If `coll` is a list returns a new list without the first
item. If `coll` is a vector, returns a new vector without the last
item. If the collection is empty, raises an error. Not the same as
`next` or `butlast`."
  [coll]
  (case (getmetatable coll)
    {:cljlib/type :seq} (case (seq coll)
                          s (drop 1 s)
                          _ (error "can't pop empty list" 2))
    {:cljlib/pop f} (f coll)
    _ (error (.. "pop is not supported on " (class coll)) 2)))

;;; Juxt

(fn* core.juxt
  "Takes a set of functions and returns a `fn` that is the juxtaposition
of those `fn`s.  The returned `fn` takes a variable number of args, and
returns a vector containing the result of applying each `fn` to the
args (left-to-right).
`((juxt a b c) x)` => `[(a x) (b x) (c x)]`"
  ([f]
   (fn*
     ([] (vector (f)))
     ([x] (vector (f x)))
     ([x y] (vector (f x y)))
     ([x y z] (vector (f x y z)))
     ([x y z & args] (vector (apply f x y z args)))))
  ([f g]
   (fn*
     ([] (vector (f) (g)))
     ([x] (vector (f x) (g x)))
     ([x y] (vector (f x y) (g x y)))
     ([x y z] (vector (f x y z) (g x y z)))
     ([x y z & args] (vector (apply f x y z args) (apply g x y z args)))))
  ([f g h]
   (fn*
     ([] (vector (f) (g) (h)))
     ([x] (vector (f x) (g x) (h x)))
     ([x y] (vector (f x y) (g x y) (h x y)))
     ([x y z] (vector (f x y z) (g x y z) (h x y z)))
     ([x y z & args] (vector (apply f x y z args) (apply g x y z args) (apply h x y z args)))))
  ([f g h & fs]
   (let [fs (core.list* f g h fs)]
     (fn*
       ([] (reduce #(conj $1 ($2)) (vector) fs))
       ([x] (reduce #(conj $1 ($2 x)) (vector) fs))
       ([x y] (reduce #(conj $1 ($2 x y)) (vector) fs))
       ([x y z] (reduce #(conj $1 ($2 x y z)) (vector) fs))
       ([x y z & args] (reduce #(conj $1 (apply $2 x y z args)) (vector) fs))))))

;;; Transients

(defn transient
  "Returns a new, transient version of the collection."
  [coll]
  (case (getmetatable coll)
    {:cljlib/editable true :cljlib/transient f} (f coll)
    _ (error "expected editable collection" 2)))

(defn conj!
  "Adds `x` to the transient collection, and return `coll`."
  ([] (transient (vec* [])))
  ([coll] coll)
  ([coll x]
   (case (getmetatable coll)
     {:cljlib/type :transient :cljlib/conj! f} (f coll x)
     {:cljlib/type :transient} (error "unsupported transient operation" 2)
     _ (error "expected transient collection" 2))
   coll))

(fn* core.assoc!
  "Remove `k`from transient map, and return `map`."
  [map k & ks]
  (case (getmetatable map)
    {:cljlib/type :transient :cljlib/dissoc! f} (apply f map k ks)
    {:cljlib/type :transient} (error "unsupported transient operation" 2)
    _ (error "expected transient collection" 2))
  map)

(fn* core.dissoc!
  "Remove `k`from transient map, and return `map`."
  [map k & ks]
  (case (getmetatable map)
    {:cljlib/type :transient :cljlib/dissoc! f} (apply f map k ks)
    {:cljlib/type :transient} (error "unsupported transient operation" 2)
    _ (error "expected transient collection" 2))
  map)

(fn* core.disj!
  "disj[oin]. Returns a transient set of the same type, that does not
contain `key`."
  ([Set] Set)
  ([Set key & ks]
   (case (getmetatable Set)
     {:cljlib/type :transient :cljlib/disj! f} (apply f Set key ks)
     {:cljlib/type :transient} (error "unsupported transient operation" 2)
     _ (error "expected transient collection" 2))))

(fn* core.pop!
  "Removes the last item from a transient vector. If the collection is
empty, raises an error Returns coll"
  [coll]
  (case (getmetatable coll)
    {:cljlib/type :transient :cljlib/pop! f} (f coll)
    {:cljlib/type :transient} (error "unsupported transient operation" 2)
    _ (error "expected transient collection" 2)))

(defn persistent!
  "Returns a new, persistent version of the transient collection. The
transient collection cannot be used after this call, any such use will
raise an error."
  [coll]
  (case (getmetatable coll)
    {:cljlib/type :transient :cljlib/persistent! f} (f coll)
    _ (error "expected transient collection" 2)))

;;; Into

(fn* core.into
  "Returns a new coll consisting of `to` with all of the items of `from`
conjoined. A transducer `xform` may be supplied.

# Examples

Insert items of one collection into another collection:

```fennel
(assert-eq [1 2 3 :a :b :c] (into [1 2 3] \"abc\"))
(assert-eq {:a 2 :b 3} (into {:a 1} {:a 2 :b 3}))
```

Transform a hash-map into a sequence of key-value pairs:

``` fennel
(assert-eq [[:a 1]] (into (vector) {:a 1}))
```

You can also construct a hash-map from a sequence of key-value pairs:

``` fennel
(assert-eq {:a 1 :b 2 :c 3}
           (into (hash-map) [[:a 1] [:b 2] [:c 3]]))
```"
  ([] (vector))
  ([to] to)
  ([to from]
   (case (getmetatable to)
     {:cljlib/editable true}
     (persistent! (reduce conj! (transient to) from))
     _ (reduce conj to from)))
  ([to xform from]
   (case (getmetatable to)
     {:cljlib/editable true}
     (persistent! (transduce xform conj! (transient to) from))
     _ (transduce xform conj to from))))

;;; Hash Set

(fn viewset [Set view inspector indent]
  (if (. inspector.seen Set)
      (.. "@set" (. inspector.seen Set) "{...}")
      (let [prefix (.. "@set"
                       (if (inspector.visible-cycle? Set)
                           (. inspector.seen Set) "")
                       "{")
            set-indent (length prefix)
            indent-str (string.rep " " set-indent)
            lines (icollect [v (pairs* Set)]
                    (.. indent-str
                        (view v inspector (+ indent set-indent) true)))]
        (tset lines 1 (.. prefix (string.gsub (or (. lines 1) "") "^%s+" "")))
        (tset lines (length lines) (.. (. lines (length lines)) "}"))
        lines)))

(fn hash-set->transient [immutable]
  (fn [hset]
    (let [removed (setmetatable {} {:__index deep-index})]
      (->> {:__index (fn [_ k]
                       (if (not (. removed k)) (. hset k)))
            :cljlib/type :transient
            :cljlib/conj #(error "can't `conj` onto transient set, use `conj!`")
            :cljlib/disj #(error "can't `disj` a transient set, use `disj!`")
            :cljlib/assoc #(error "can't `assoc` onto transient set, use `assoc!`")
            :cljlib/dissoc #(error "can't `dissoc` onto transient set, use `dissoc!`")
            :cljlib/conj! (fn [thset v]
                            (if (= nil v)
                                (tset removed v true)
                                (tset removed v nil))
                            (doto thset (tset v v)))
            :cljlib/assoc! #(error "can't `assoc!` onto transient set")
            :cljlib/assoc! #(error "can't `dissoc!` a transient set")
            :cljlib/disj! (fn [thset ...]
                            (for [i 1 (select "#" ...)]
                              (let [k (select i ...)]
                                (tset thset k nil)
                                (tset removed k true)))
                            thset)
            :cljlib/persistent! (fn [thset]
                                  (let [t (collect [k v (pairs thset)
                                                    :into (collect [k v (pairs hset)]
                                                            (values k v))]
                                            (values k v))]
                                    (each [k (pairs removed)]
                                      (tset t k nil))
                                    (each [_ k (ipairs (icollect [k (pairs* thset)] k))]
                                      (tset thset k nil))
                                    (setmetatable thset
                                                  {:__index #(error "attempt to use transient after it was persistet")
                                                   :__newindex #(error "attempt to use transient after it was persistet")})
                                    (immutable (itable t))))}
           (setmetatable {})))))

(fn hash-set* [x]
  (case (getmetatable x)
    mt (doto mt
         (tset :cljlib/type :hash-set)
         (tset :cljlib/conj
               (fn [s v ...]
                 (hash-set*
                  (itable.assoc
                   s v v
                   (unpack* (let [res []]
                              (each [ _ v (ipairs [...])]
                                (table.insert res v)
                                (table.insert res v))
                              res))))))
         (tset :cljlib/disj
               (fn [s k ...]
                 (let [to-remove
                       (collect [_ k (ipairs [...])
                                 :into (->> {:__index deep-index}
                                            (setmetatable {k true}))]
                         k true)]
                   (hash-set*
                    (itable.assoc {}
                                  (unpack*
                                   (let [res []]
                                     (each [_ v (pairs s)]
                                       (when (not (. to-remove v))
                                         (table.insert res v)
                                         (table.insert res v)))
                                     res)))))))
         (tset :cljlib/empty #(hash-set* (itable {})))
         (tset :cljlib/editable true)
         (tset :cljlib/transient (hash-set->transient hash-set*))
         (tset :cljlib/seq (fn [s] (map #(if (vector? $) (. $ 1) $) s)))
         (tset :__fennelview viewset)
         (tset :__fennelrest (fn [s i]
                               (var j 1)
                               (let [vals []]
                                 (each [v (pairs* s)]
                                   (if (>= j i)
                                       (table.insert vals v)
                                       (set j (+ j 1))))
                                 (core.hash-set (unpack* vals))))))
    _ (hash-set* (setmetatable x {})))
  x)

(fn* core.hash-set
  "Create hash set.

Set is a collection of unique elements, which sore purpose is only to
tell you if something is in the set or not."
  [& xs]
  (let [Set (collect [_ val (pairs* xs)
                      :into (->> {:__newindex deep-newindex}
                                 (setmetatable {}))]
              (values val val))]
    (hash-set* (itable Set))))

;;; Multimethods

(defn multifn?
  "Test if `mf' is an instance of `multifn'.

`multifn' is a special kind of table, created with `defmulti' macros
from `macros.fnl'."
  [mf]
  (case (getmetatable mf)
    {:cljlib/type :multifn} true
    _ false))

(fn* core.remove-method
  "Remove method from `multimethod' for given `dispatch-value'."
  [multimethod dispatch-value]
  (if (multifn? multimethod)
      (tset multimethod dispatch-value nil)
      (error (.. (tostring multimethod) " is not a multifn") 2))
  multimethod)

(fn* core.remove-all-methods
  "Removes all methods of `multimethod'"
  [multimethod]
  (if (multifn? multimethod)
      (each [k _ (pairs multimethod)]
        (tset multimethod k nil))
      (error (.. (tostring multimethod) " is not a multifn") 2))
  multimethod)

(fn* core.methods
  "Given a `multimethod', returns a map of dispatch values -> dispatch fns"
  [multimethod]
  (if (multifn? multimethod)
      (let [m {}]
        (each [k v (pairs multimethod)]
          (tset m k v))
        m)
      (error (.. (tostring multimethod) " is not a multifn") 2)))

(fn* core.get-method
  "Given a `multimethod' and a `dispatch-value', returns the dispatch
`fn' that would apply to that value, or `nil' if none apply and no
default."
  [multimethod dispatch-value]
  (if (multifn? multimethod)
      (or (. multimethod dispatch-value)
          (. multimethod :default))
      (error (.. (tostring multimethod) " is not a multifn") 2)))

;;; Inst

(fn* core.inst
  "Parses `s` as a subset of ISO-8601 and returns a table with the
following keys: year, month, day, hour, min, sec, msec.
This table can be used with `os.time`."
  [s]
  (let [date (lua-inst s)]
    (doto (getmetatable date)
      (tset :cljlib/type :inst)
      (tset :__fennelview
        #(: "#<inst: %04d-%s.%03d-00:00>" :format $.year (os.date "%m-%dT%H:%M:%S" (os.time $)) $.msec)))
    date))

(fn* core.inst?
  "Return true if `x` is an `inst`."
  [x]
  (case (getmetatable x)
    {:cljlib/type :inst} true
    _ false))

(fn* core.inst-ms
  "Return the number of milliseconds for the given `inst`"
  [inst]
  (assert (core.inst? inst) "Expected a date instance")
  (+ (* (os.time inst) 1000) inst.msec))

;;; Agents

(fn start-task-runner [agent]
  "Spawns a go-thread, running while `agent` is ready.
Takes tasks from the queue, applies the validator and handles errors
if any."
  (a.go* #(while (= agent.status :ready)
            (let [[f args] (a.<! agent.tasks)]
              (let [(task-ok? state-or-msg) (pcall apply f agent.state args)
                    (task-ok? state-or-msg)
                    (if task-ok?
                        (let [(validator-ok err)
                              (pcall agent.validator state-or-msg)]
                          (if validator-ok
                              (values true state-or-msg)
                              (values false err)))
                        (values false state-or-msg))]
                (case (values task-ok? state-or-msg)
                  (true ?state) (set agent.state ?state)
                  (false err) (do (when (not= agent.error-mode :continue)
                                    (set agent.status :failed)
                                    (set agent.error err))
                                  (when agent.error-handler
                                    (agent.error-handler agent err))))))))
  nil)

(var agents {})

(fn* core.agent
  "Creates and returns an `agent` with an initial value of `state` and
zero or more `options` (in any order):

:validator validate-fn
:error-handler handler-fn
:error-mode mode-keyword

`validate-fn` must be `nil` or a side-effect-free `fn` of one
argument, which will be passed the intended new state on any state
change. If the new state is unacceptable, the `validate-fn` should
return `false` or raise an error.  `handler-fn` is called if an action
raises an error or if `validate-fn` rejects a new state.  See
`set-error-handler!` for details.  The `mode-keyword` may be either
`:continue` (the default if an `:error-handler` is given) or
`:fail` (the default if no `:error-handler` is given). See
`set-error-mode!` for details."
  [state & options]
  (let [opts (apply hash-map options)
        agent (setmetatable
               {: state
                :error-mode (or opts.error-mode
                                (and opts.error-handler :continue)
                                :fail)
                :og-validator opts.validator
                :validator (if opts.validator
                               (fn [newstate]
                                 (when (not (opts.validator newstate))
                                   (error "Invalid reference state" 2)))
                               #true)
                :error-handler (or opts.error-handler (fn [] true))
                :status :ready
                :tasks (a.chan)}
               {:cljlib/type :agent
                :cljlib/deref (fn [self] self.state)
                :cljlib/send (fn [agent task]
                               (when (not= agent.status :ready)
                                 (error agent.error 3))
                               (a.go* #(a.>! agent.tasks task))
                               agent)
                :__fennelview (fn [agent view inspector indent]
                                (let [prefix (.. "#<" (string.gsub (tostring agent) "table" "agent") " ")]
                                  [(.. prefix
                                       (view {:val agent.state :status agent.status}
                                             (doto inspector (tset :one-line? true))
                                             (+ indent (length prefix)))
                                       ">")]))})]
    (start-task-runner agent)
    (tset agents agent true)
    agent))

(fn* core.set-error-handler!
  "Sets the error-handler of `agent` a to `handler-fn`.  If an action
being run by the `agent` raises an error or doesn't pass the validator
fn, `handler-fn` will be called with two arguments: the `agent` and
the error."
  [agent handler-fn]
  (set agent.error-handler handler-fn))

(fn* core.error-handler
  "Returns the error-handler of agent `a`, or `nil` if there is none.
See `set-error-handler!`"
  [a]
  a.error-handler)

(fn* core.set-error-mode!
  "Sets the error-mode of `agent` a to `mode-keyword`, which must be
either `:fail` or `:continue`.  If an action being run by the agent
raises an error or doesn't pass the validator fn, an error-handler may
be called (see `set-error-handler!`), after which, if the mode is
`:continue`, the agent will continue as if neither the action that
caused the error nor the error itself ever happened.

If the mode is `:fail`, the agent will become failed and will stop
accepting new `send` actions, and any previously queued actions will
be held until a `restart-agent`.  `deref` will still work, returning
the state of the `agent` before the error."
  [agent mode-keyword]
  (set agent.error-mode mode-keyword))

(fn* core.error-mode
  "Returns the error-mode of agent `a`.  See `set-error-mode!`"
  [a]
  a.error-mode)

(fn* core.agent-error
  "Returns the error raised during an asynchronous action of the
`agent` if the `agent` is failed.  Returns `nil` if the `agent` is not
failed."
  [agent]
  agent.error)

(fn* core.restart-agent
  "When an `agent` is failed, changes the agent state to `new-state` and
then un-fails the agent so that sends are allowed again.  If `options`
contained the `:clear-actions` option, any actions queued on the agent
that were being held while it was failed will be discarded, otherwise
those held actions will proceed.  The `new-state` must pass the
validator if any, or restart will raise an error and the `agent` will
remain failed with its old state and error. Raises an error if the
`agent` is not failed."
  [agent new-state & options]
  (when (not= agent.status :failed)
    (error "Agent does not need a restart" 2))
  (let [opts (apply hash-map options)]
    (doto agent
      (tset :state new-state)
      (tset :status :ready)
      (tset :error nil))
    (when (. opts :clear-actions)
      (set agent.tasks (a.chan)))
    (start-task-runner agent)))

(defn send
  "Dispatch an action to an agent `a`. Returns the agent immediately.
Subsequently, in agent's task pool, the state of the agent will be set
to the value of: `(apply f state-of-agent args)`"
  [a f & args]
  (case (getmetatable a)
    {:cljlib/send send} (send a [f args])
    _ (error "object doesn't implement cljlib/send metamethod" 2))
  a)

(fn* core.shutdown-agents
  "Initiates a shutdown of the task threads that back the agent
system."
  []
  (each [agent (pairs agents)]
    (set agent.status nil)
    (send a (fn [x] x)))
  (set agents {}))

(fn core.await [...]
  "Blocks the current thread (indefinitely!) until all actions
dispatched thus far, from this thread or agent, to the `agents` have
occurred.  Will block on failed agents.  Will never return if a failed
agent is restarted with `:clear-actions` `true` or `shutdown-agents`
was called."
  {:fnl/arglist [& agents]}
  (io! "await in transaction"
       (let [latch {:n (select :# ...)}
             count-down (fn [agent] (set latch.n (- latch.n 1)) agent)]
         (for [i 1 (select :# ...)]
           (let [a (select i ...)]
             (core.send a count-down)))
         (while (not= latch.n 0)
           nil))))

(fn core.await-for [timeout ...]
  "Blocks the current thread until all actions dispatched thus
far (from this thread or agent) to the `agents` have occurred, or the
`timeout` (in milliseconds) has elapsed. Returns logical `false` if
returning due to timeout, logical `true` otherwise."
  {:fnl/arglist [timeout & agents]}
  (io! "await in transaction"
       (let [latch {:n (select :# ...)}
             count-down (fn [agent] (set latch.n (- latch.n 1)) agent)]
         (for [i 1 (select :# ...)]
           (let [a (select i ...)]
             (core.send a count-down)))
         (a.<!! (a.timeout timeout))
         (= latch.n 0))))

;; Atom & volatile

(fn* core.atom
  "Creates and returns an Atom with an initial value of `x` and zero or
more `options`:

:validator validate-fn

`validate-fn` must be `nil` or a side-effect-free `fn` of one
argument, which will be passed the intended new state on any state
change. If the new state is unacceptable, the `validate-fn` should
return false or raise an error.

*Note*: unlike Clojure, atoms are not atomic, because the runtime only
has a single thread."
  [x & options]
  (let [opts (apply hash-map options)]
    (when (and opts.validator
               (not (opts.validator x)))
      (error "Invalid reference state" 2))
    (setmetatable
     {:val x
      :og-validator opts.validator
      :validator (if opts.validator
                     (fn [newstate]
                       (when (not (opts.validator newstate))
                         (error "Invalid reference state" 2)))
                     (fn [_newstate] true))}
     {:cljlib/type :atom
      :cljlib/deref (fn [self] self.val)
      :__fennelview (fn [atom view inspector indent]
                      (let [prefix (.. "#<" (string.gsub (tostring atom) "table" "atom") " ")]
                        [(.. prefix
                             (view {:val atom.val :status :ready}
                                   (doto inspector (tset :one-line? true))
                                   (+ indent (length prefix)))
                             ">")]))})))

(fn core.swap! [atom f ...]
  "Swaps the value of `atom` to be: `(apply f current-value-of-atom args)`.
Returns the value that was swapped in."
  {:fnl/arglist [atom f & args]}
  (let [newval (f atom.val ...)]
    (atom.validator newval)
    (set atom.val newval)
    newval))

(fn* core.reset!
  "Sets the value of `atom` to `newval` without regard for the
current value. Returns `newval`."
  [atom newval]
  (core.swap! atom #newval))

(fn* core.compare-and-set!
  "Sets the value of `atom` to `newval` if and only if the
current value of the atom is identical to `oldval`. Returns `true` if
set happened, else `false`."
  [atom oldval newval]
  (let [curval (deref atom)]
    (if (eq curval oldval)
        (do (core.reset! atom newval)
            true)
        false)))

(fn* core.reset-vals!
  "Sets the value of `atom` to `newval`. Returns `[old new]`, the value
of the atom before and after the reset."
  [atom newval]
  (let [oldval (deref atom)]
    [oldval (core.reset! atom newval)]))

(fn core.swap-vals! [atom f ...]
  "Atomically swaps the value of `atom` to be: `(apply f
current-value-of-atom args)`. Returns `[old new]`, the value of the
`atom` before and after the swap."
  {:fnl/arglist [atom f & args]}
  (let [oldval (deref atom)]
    [oldval (core.swap! atom f ...)]))

(fn* core.volatile!
  "Creates and returns a Volatile with an initial value of `x`.

*Note*: The only difference from `atom` is the absense of a validator."
  [x]
  (setmetatable
   {:val x}
   {:cljlib/type :volatile
    :cljlib/deref (fn [self] self.val)
    :__fennelview (fn [volatile view inspector indent]
                    (let [prefix (.. "#<" (string.gsub (tostring volatile) "table" "volatile") " ")]
                      [(.. prefix
                           (view {:val volatile.val :status :ready}
                                 (doto inspector (tset :one-line? true))
                                 (+ indent (length prefix)))
                           ">")]))}))

(fn core.vswap! [volatile f ...]
  "Swaps the value of the `volatile` as if: `(apply f current-value-of-vol args)`.
Returns the value that was swapped in."
  (let [newval (f volatile.val ...)]
    (set volatile.val newval)
    newval))

(fn* core.vreset!
  "Sets the value of `volatile` to `newval` without regard for the
current value. Returns newval."
  [volatile newval]
  (core.vswap! volatile #newval))

(fn* core.set-validator!
  "Sets the `validator-fn` for a `agent/atom`. `validator-fn` must be `nil` or a
side-effect-free `fn` of one argument, which will be passed the
intended new state on any state change. If the new state is
unacceptable, the `validator-fn` should return `false` or raise an
error. If the current state is not acceptable to the new validator, an
error will be raised and the validator will not be changed."
  [agent/atom validator-fn]
  (when (and (not= nil validator-fn)
             (not (validator-fn (deref agent/atom))))
    (error "Invalid reference state" 2))
  (set agent/atom.og-validator validator-fn)
  (set agent/atom.validator
    (fn [newstate]
      (when (not (validator-fn newstate))
        (error "Invalid reference state" 2)))))

(fn* core.get-validator
  "Gets the validator-fn for a `agent/atom`."
  [agent/atom]
  agent/atom.og-validator)

;;; Tap

(local tap-ch (a.chan))
(local mult-ch (a.mult tap-ch))
(local taps {})

(defn remove-tap
  "Remove `f` from the tap set."
  [f]
  (case (. taps f)
    c (do (a.untap mult-ch c)
          (a.close! c)))
  (tset taps f nil))

(fn* core.add-tap
  "Adds `f`, a fn of one argument, to the tap set. This function will be
called with anything sent via `tap>`.  This function may (briefly)
block, and will never impede calls to `tap>`, but blocking
indefinitely may cause program to wait indefinitely.  Remember `f` in
order to `remove-tap`."
  [f]
  (remove-tap f)
  (let [c (a.chan)]
    (a.tap mult-ch c)
    (a.go*
     #((fn recur []
         (let [data (a.<! c)]
           (when data
             (f data)
             (recur))))))
    (tset taps f c)))

(fn* core.tap>
  "Sends `x` to any taps. Will not block. Returns `true` if there was
room in the queue, `false` if not (dropped)."
  [x]
  (a.offer! tap-ch x))

;; UUID

(fn* core.random-uuid
  "Returns a pseudo-randomly generated UUID instance (i.e. type 4)."
  []
  (uuid.random-uuid))

(fn* core.uuid?
  "Return `true` if `x` is a string representing a UUID."
  [x]
  (uuid.uuid? x))

core
