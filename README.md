# Fennel Cljlib

Experimental library for the [Fennel](https://fennel-lang.org/) language, that adds many functions from [Clojure](https://clojure.org/)'s standard library.
This is not a one-to-one port of Clojure `core`, because many Clojure features require certain facilities from the runtime.
This library implements lazy sequences, transducers, immutable tables, sets and vectors, transients, and a lot of functions from the `core` namespace.
Some semantics like dynamic scope and parallelism are not supported by Lua runtime at all.
Therefore, certain functions were altered to better suit the domain or omitted entirely.

## Installation

### Via [deps.fnl](https://gitlab.com/andreyorst/deps.fnl)

Add this to the `deps.fnl` file in your project:

```clojure
{:deps {"http://gitlab.com/andreyorst/fennel-cljlib"
        {:type :git :sha "f20d89b96ebfc8171f8d7eea8afc801c2fbbe4bf"}}}
```

## Usage

This library provides both functions and macros from a single module:

```clojure
(local core
  (require :io.gitlab.andreyorst.cljlib.core))
```

To use macros, simply use `import-macros` on `:io.gitlab.andreyorst.cljlib.core`:

```clojure
(import-macros {: defn} :io.gitlab.andreyorst.cljlib.core)
```

### `core.async`

Some facilities from Clojure's [`core.async`][5] library are available with `cljlib` through the [async.fnl][6] dependency.

```clojure
(import-macros {: def} :io.gitlab.andreyorst.cljlib.core)
(local core (require :io.gitlab.andreyorst.cljlib.core))

(local a (require :io.gitlab.andreyorst.cljlib.core.async))
(import-macros {: go} :io.gitlab.andreyorst.cljlib.core.async)

(def ch (a.chan 1 (core.map core.inc)))

(go (a.>! ch 41))
(a.<!! (go (a.alts! [ch (a.timeout 1000)])))
;; [42 #<ManyToManyChannel: 0x557b64781cd0>]
```

Consult the [async.fnl documentation][7] for more information.

### Agents

Agents are implemented in terms of `async.fnl`.

This library provides agents mainly as a mean of porting Clojure code that uses agents.
However, because the Lua runtime is single-threaded, tasks sent to agents should never block.
Agent's tasks are ran in the context of a `go` block, so any `core.async` code should work inside of an agent's task:

```clojure
>> (local core (require :io.gitlab.andreyorst.cljlib.core))
nil
>> (local a (require :io.gitlab.andreyorst.cljlib.core.async))
nil
>> (require-macros :io.gitlab.andreyorst.cljlib.core)
nil
>> (time
    (let [agent (core.agent 41)]
      (core.send agent (fn [] (a.<! (a.timeout 900)) 42))
      (a.<!! (a.timeout 1000))
      agent))
Elapsed time: 1009.4449520111 msecs
#<agent 0x557d2e7db4c0: {:status "ready" :val 42}>
```

## Documentation

Documentation is auto-generated with [Fenneldoc][2] and can be found [here][3].

# Contributing

Please make sure you've read [contribution guidelines][4].

Tests can be ran with:

    deps tasks/run-tests --merge dev-deps.fnl

[1]: https://gitlab.com/andreyorst/fennel-cljlib/-/raw/master/cljlib.fnl
[2]: https://gitlab.com/andreyorst/fenneldoc
[3]: https://gitlab.com/andreyorst/fennel-cljlib/-/blob/master/doc/cljlib.md
[4]: https://gitlab.com/andreyorst/fennel-cljlib/-/tree/master/CONTRIBUTING.md
[5]: https://github.com/clojure/core.async
[6]: https://gitlab.com/andreyorst/async.fnl
[7]: https://gitlab.com/andreyorst/async.fnl/-/blob/main/doc/src/async.md

<!--  LocalWords:  Lua submodule precompile cljlib docstring config
      LocalWords:  namespace destructure runtime Clojure precompiled
 -->
