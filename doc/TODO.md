# TODO

The following functions from `clojure.core` are yet to be implemented. <sub><sub><sub>maybe</sub></sub></sub>

## Metadata
- `alter-meta!`
- `meta`
- `reset-meta!`
- `type` (`:type` metadata)
- `vary-meta`
- `with-meta`

## Macros
- `..`
- `amap`
- `areduce` (array)
- `as->`
- `case` (?)
- `cond->>`
- `cond->`
- `condp`
- `defstruct`
- `delay`
- `doseq`
- `dosync`
- `dotimes`
- `for`
- `io!` (STM)
- `letfn`
- `memfn`
- `some->>`
- `some->`
- `sync`
- `with-precision`
- `with-redefs`

## Numerical
- `decimal?`
- `denominator`
- `numerator`
- `ratio?`
- `rational?`
- `rationalize`

### Arbitrary nummbers
- `*'`
- `+'`
- `-'`
- `bigdec`
- `bigint`
- `biginteger`
- `dec'`
- `inc'`
- `unchecked-add-int`
- `unchecked-add`
- `unchecked-dec-int`
- `unchecked-dec`
- `unchecked-divide-int`
- `unchecked-inc-int`
- `unchecked-inc`
- `unchecked-multiply-int`
- `unchecked-multiply`
- `unchecked-negate-int`
- `unchecked-negate`
- `unchecked-remainder-int`
- `unchecked-subtract-int`
- `unchecked-subtract`

### Fractions
`/` (return a proper fraction when both numbers are ints)

## Sorted collections
- `sort-by`
- `sorted-map-by`
- `sorted-map`
- `sorted-set-by`
- `sorted-set`
- `sorted?`

## Arrays
- `Structs`
- `accessor`
- `aclone`
- `aget`
- `alength`
- `array-map`
- `aset-boolean`
- `aset-byte`
- `aset-char`
- `aset-double`
- `aset-float`
- `aset-int`
- `aset-long`
- `aset-short`
- `aset`
- `boolean-array`
- `booleans` (array cast)
- `byte-array`
- `bytes?`
- `bytes` (array cast)
- `char-array`
- `chars` (array cast)
- `create-struct`
- `double-array`
- `doubles` (array cast)
- `float-array`
- `floats` (array cast)
- `int-array`
- `into-array`
- `ints` (array cast)
- `long-array`
- `longs` (array cast)
- `make-array`
- `object-array`
- `short-array`
- `shorts` (array cast)
- `struct-map`
- `struct`
- `to-array-2d`
- `to-array`

## Dynamic scope
- `alter-var-root`
- `binding`
- `bound-fn*`
- `bound-fn`
- `bound?`
- `thread-bound?`
- `with-bindings*`
- `with-bindings`

## Keywords
- `ancestors`
- `bases`
- `derive`
- `descendants`
- `find-keyword`
- `ident?`
- `isa?`
- `keyword?`
- `make-hierarchy`
- `name`
- `namespace`
- `parents`
- `qualified-ident?`
- `qualified-keyword?`
- `simple-ident?`
- `simple-keyword?`
- `supers`
- `underive` (also classes)

## Multimethods
- `prefer-method`
- `prefers`

## STM
- `alter`
- `commute`
- `ensure`
- `ref-history-count`
- `ref-max-history`
- `ref-min-history`
- `ref-set`
- `ref`

## Coersions
- `byte`
- `char`
- `double`
- `float`
- `long`
- `num`
- `short`
- `unchecked-byte`
- `unchecked-char`
- `unchecked-double`
- `unchecked-float`
- `unchecked-int`
- `unchecked-long`
- `unchecked-short`

## IO
- `read-line`
- `read-string`
- `read`
- `slurp`
- `spit`

## Namespaces
- `alias`
- `all-ns`
- `create-ns`
- `find-ns`
- `ns-aliases`
- `ns-imports`
- `ns-interns`
- `ns-map`
- `ns-name`
- `ns-publics`
- `ns-refers`
- `ns-resolve`
- `ns-unalias`
- `ns-unmap`
- `refer`
- `remove-ns`
- `resolve`
- `the-ns`

## Regular expressions
- `re-find`
- `re-groups`
- `re-matcher`
- `re-matches`
- `re-pattern`
- `re-seq`

## Sequences/Collections
- `bounded-count`
- `cat`
- `coll?`
- `completing` (reduce)
- `counted? `
- `distinct?`
- `eduction`
- `ensure-reduced`
- `enumeration-seq`
- `every-pred`
- `file-seq`
- `indexed?`
- `iterator-seq`
- `key`
- `map-entry?`
- `max-key`
- `merge-with`
- `min-key`
- `nth`
- `peek`
- `rand-nth`
- `resultset-seq`
- `reversible?`
- `rsubseq`
- `run!`
- `select-keys`
- `seqable?`
- `seque`
- `sequence`
- `sequential?`
- `shuffle`
- `subseq`
- `subvec`
- `val`
- `xml-seq`

## Futures/delays
- `delay?`
- `deliver`
- `force`
- `future-call`
- `future-cancel`
- `future-cancelled?`
- `future-done?`
- `future?`
- `future` (think about making it via async.fnl)
- `pcalls`
- `pmap`
- `promise`
- `pvalues`

## Uncategorized
- `add-watch`
- `cast`
- `class?`
- `comparator`
- `compare`
- `hash-ordered-coll`
- `hash-unordered-coll`
- `hash`
- `instance?`
- `intern` (?)
- `load-reader` (?)
- `load-string` (?)
- `mix-collection-hash`
- `release-pending-sends` (agents)
- `remove-watch`
- `some-fn`
- `subs`
- `trampoline`
- `uri?`
- `with-redefs-fn`
