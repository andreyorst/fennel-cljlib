{:deps {:io.gitlab.andreyorst/async.fnl
        {:type :git :sha "ea0a63f2c87651f9c63ee775f2a066281b868573"}
        :io.gitlab.andreyorst/lua-inst
        {:type :git :sha "98618824065f8176c6ba5700549e9604fbb1cc58"}
        :io.gitlab.andreyorst/lazy-seq
        {:type :git :sha "c7148adc558097d3978674fcf27a6c622499259d"}
        :io.gitlab.andreyorst/reduced.lua
        {:type :git :sha "92cc61fee3250cb3eb54cc7b6f41f9625f7114bc"}
        :io.gitlab.andreyorst/itable
        {:type :git :sha "e3aeb6264b87ccd8dce70bdee9f628a9fef78178"}
        :io.gitlab.andreyorst/uuid.fnl
        {:type :git :sha "209ff4f3a70ba8354034eaf90b70ddb0d14ea254"}
        :io.gitlab.andreyorst/reader.fnl
        {:type :git :sha "515c2695fad06d01f279c92d91e9e530a4a427e7"}}
 :paths {:fennel ["src/?/init.fnl" "src/?.fnl"]
         :macro ["src/?/init.fnlm" "src/?.fnlm"]}
 :profiles
 {:dev
  {:deps {"https://gitlab.com/andreyorst/fennel-test"
          {:type :git :sha "416895275e4f20c1c00ba15dd0e274d1b459c533"}}
   :paths {:fennel ["tests/?.fnl"]}}
  :doc
  {:deps {"fennel" {:type :rock :version "1.5.1-1"}}
   :paths {:fennel ["tasks/?"]}}}}
