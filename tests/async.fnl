(require-macros :io.gitlab.andreyorst.fennel-test)
(local {: eq} (require :io.gitlab.andreyorst.fennel-test))

(local a (require :io.gitlab.andreyorst.cljlib.core.async))
(local core (require :io.gitlab.andreyorst.cljlib.core))

(deftest xform-test
  (testing "transducers work with channels"
    (let [ch (a.chan 1 (core.map core.inc))]
      (a.>!! ch 41)
      (assert-eq 42 (a.<!! ch)))))

(fn state-changed? [state agent ?iterations]
  (for [i 1 100000000
        :until (not (eq state (core.deref agent)))]
    nil)
  (not (eq state (core.deref agent))))

(fn status-changed? [status agent ?iterations]
  (for [i 1 100000000
        :until (eq status agent.status)]
    nil)
  (eq status agent.status))

(deftest agent-test
  (testing "agent changes state"
    (let [state 41
          agent (core.agent state)]
      (core.send agent core.inc)
      (assert-is (state-changed? state agent))
      (assert-eq 42 (core.deref agent))))
  (testing "agent keeps state after failure"
    (let [state 41
          agent (core.agent state)]
      (core.send agent error)
      (assert-is (status-changed? :failed agent))))
  (testing "agent throws on send after failure"
    (let [state 41
          agent (core.agent state)]
      (core.send agent error)
      (assert-is (status-changed? :failed agent))
      (assert-not (pcall core.send agent core.inc) "agent did not throw on send")))
  (testing "agent keeps working with error-mode :continue"
    (var handled false)
    (let [state 41
          agent (core.agent state :error-handler (fn [...] (set handled [...])))]
      (assert-eq agent.error-mode :continue)
      (core.send agent error)
      (assert-not (status-changed? :failed agent))
      (assert-eq handled [agent 41])
      (core.send agent core.inc)
      (assert-is (state-changed? state agent))
      (assert-eq 42 (core.deref agent))))
  (testing "agent uses error handler"
    (var handled false)
    (let [state 41
          agent (core.agent state)]
      (core.set-error-handler! agent (fn [...] (set handled [...])))
      (assert-eq agent.error-mode :fail)
      (core.send agent error)
      (assert-is (status-changed? :failed agent))
      (assert-eq handled [agent 41])))
  (testing "setting agent validator raises an error"
    (let [agent (core.agent 42)]
      (assert-not (pcall core.set-validator! agent core.odd?))))
  (testing "setting agent validator works"
    (let [agent (core.agent 42)]
      (assert-is (pcall core.set-validator! agent core.even?))
      (var handled false)
      (core.set-error-handler! agent (fn [...] (set handled [...])))
      (core.send agent core.inc)
      (assert-is (status-changed? :failed agent))
      (assert-eq handled [agent "Invalid reference state"])))
  (testing "waiting for agents"
    (let [agents (core.mapv core.agent (core.range 10))]
      (each [_ agent (ipairs agents)]
        (core.send agent (fn [x] (a.<! (a.timeout 1000)) (+ x 1))))
      (core.apply core.await agents)
      (assert-eq (core.mapv core.deref agents)
                 (core.range 1 11))))
  (testing "waiting for agents with a timeout"
    (let [agents (core.mapv core.agent (core.range 10))]
      (each [_ agent (ipairs agents)]
        (core.send agent (fn [x] (a.<! (a.timeout 2000)) (+ x 1))))
      (assert-not (core.apply core.await-for 1000 agents))
      (core.apply core.await agents)
      (assert-eq (core.mapv core.deref agents)
                 (core.range 1 11)))))
